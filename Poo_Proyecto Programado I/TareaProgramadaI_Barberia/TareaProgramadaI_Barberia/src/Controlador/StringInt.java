
package Controlador;


public class StringInt {
    
    String entrada;
    
    public StringInt(String pEntrada) {
        entrada = pEntrada;
    }
    
    public int pasar_a_Int() {
        int stringNumerico = 0;
        try {
            stringNumerico = java.lang.Integer.parseInt(entrada);
        }
        catch(NumberFormatException nfe){}
        return stringNumerico;
        // Si devuelve un 0, no se pudo pasar a int.
        // De lo contrario devuelve el numero con tipo de dato int.
    }
    
}
