
package Controlador;

import Objetos.Cita;
import Objetos.Cliente;
import Objetos.Dia;
import Objetos.Correo;
import Objetos.Servicio;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JOptionPane;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


public class Control_Barberia implements Serializable{
    
    private ArrayList <Cliente> listaClientes = new ArrayList();
    private ArrayList <Servicio> listaServicios = new ArrayList();
    private ArrayList <Dia> listaHorarios = new ArrayList();
    private ArrayList <Cliente> listaEspera = new ArrayList();
            
    public Control_Barberia() {
        for (int i = 0; i < 8; i++){
            Dia diaSemana = new Dia("12:00 am", "11:00 pm");
            listaHorarios.add(diaSemana);
        }
        
        //cargar Clientes
        try { 
            ObjectInputStream is = new ObjectInputStream(new FileInputStream("clientes.bin"));
            ArrayList <Cliente> arregloCu = (ArrayList) is.readObject();
            listaClientes = arregloCu;
        }
        catch (FileNotFoundException e ) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        // Cargar servicios.
        try { 
            ObjectInputStream is = new ObjectInputStream(new FileInputStream("servicios.bin"));
            ArrayList <Servicio> arregloCu = (ArrayList) is.readObject();
            listaServicios = arregloCu;
        }
        catch (FileNotFoundException e ) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        // Cargar horarios.
        try { 
            ObjectInputStream is = new ObjectInputStream(new FileInputStream("horarios.bin"));
            ArrayList <Dia> arregloCu = (ArrayList) is.readObject();
            listaHorarios = arregloCu;
        }
        catch (FileNotFoundException e ) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //cargar listaEspera
        try { 
            ObjectInputStream is = new ObjectInputStream(new FileInputStream("listaEspera.bin"));
            ArrayList <Cliente> arregloCu = (ArrayList) is.readObject();
            listaEspera = arregloCu;
        }
        catch (FileNotFoundException e ) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }     
    }
    
    public void modificarHorario(int indiceDia, String horaInicio, String horaFin){
        listaHorarios.get(indiceDia).setInicioHorario(horaInicio);
        listaHorarios.get(indiceDia).setFinHorario(horaFin);
        JOptionPane.showMessageDialog(null, "El horario fue guardado");
        save();
    }
    
    public boolean registrarCliente(String nombre, String apellido1, String apellido2, String correo, String numero) {
        boolean seRegistro = true;
        if (nombre.equals("") || apellido1.equals("") || apellido2.equals("") || correo.equals("") || numero.equals("")) {
            JOptionPane.showMessageDialog(null, "No se permiten espacios en blanco.");
            seRegistro = false;
            
        }else {
            if (correo.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") == false){
                    JOptionPane.showMessageDialog(null, "Debe ingresar un correo valido.");
                    seRegistro = false;
            }
            else {
                StringInt numeroTelefono = new StringInt(numero);
                int telefono = numeroTelefono.pasar_a_Int();
                if (telefono == 0) {
                    JOptionPane.showMessageDialog(null, "El telefono ingresado debe corresponder a un numero.");
                    seRegistro = false;
                }
                else {
                    boolean repetido = false;
                    for (int i = 0; i < listaClientes.size(); i++) {
                        if (correo.equals(listaClientes.get(i).getCorreo())) {
                            JOptionPane.showMessageDialog(null, "El correo ingresado pertenece a otro cliente.");
                            repetido = true;
                            break;
                        }
                    }
                    if (repetido == false) {
                        for (int i = 0; i < listaClientes.size(); i++) {
                            if (telefono == listaClientes.get(i).getTelefono()) {
                                JOptionPane.showMessageDialog(null, "El numero de telefono ingresado pertenece a otro cliente.");
                                repetido = true;
                                break;
                            }
                        }
                        if (repetido == false) {
                            Cliente nuevoCliente = new Cliente(nombre, apellido1, apellido2, correo, telefono);
                            listaClientes.add(nuevoCliente);
                            JOptionPane.showMessageDialog(null, "Se registro el cliente con exito.");
                            save();
                            seRegistro = true;
                        }
                    }
                }
            }
        }
    return seRegistro;
    }
    
    public boolean modificarCliente(int posicion, String nombre, String apellido1, String apellido2, String correo, String numero) {
        boolean fueModificado = false;
        if (nombre.equals("") || apellido1.equals("") || apellido2.equals("") || correo.equals("") || numero.equals("")) {
            JOptionPane.showMessageDialog(null, "No se permiten espacios en blanco.");
        }
        else {
            if (correo.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$") == false){
                    JOptionPane.showMessageDialog(null, "Debe ingresar un correo valido.");
            }
            else {
                StringInt numeroTelefono = new StringInt(numero);
                int telefono = numeroTelefono.pasar_a_Int();
                if (telefono == 0) {
                    JOptionPane.showMessageDialog(null, "El telefono ingresado debe corresponder a un numero.");
                }
                else {
                    boolean repetido = false;
                    for (int i = 0; i < listaClientes.size(); i++) {
                        if (posicion != i) {
                            if (correo.equals(listaClientes.get(i).getCorreo())) {
                                JOptionPane.showMessageDialog(null, "El correo ingresado pertenece a otro cliente.");
                                repetido = true;
                                break;
                            }
                        }
                    }
                    if (repetido == false) {
                        for (int i = 0; i < listaClientes.size(); i++) {
                            if (posicion != i) {
                                if (telefono == listaClientes.get(i).getTelefono()) {
                                    JOptionPane.showMessageDialog(null, "El numero de telefono ingresado pertenece a otro cliente.");
                                    repetido = true;
                                    break;
                                }
                            }
                        }
                        if (repetido == false) {
                            listaClientes.get(posicion).setNombre(nombre);
                            listaClientes.get(posicion).setApellido1(apellido1);
                            listaClientes.get(posicion).setApellido2(apellido2);
                            listaClientes.get(posicion).setCorreo(correo);
                            listaClientes.get(posicion).setTelefono(telefono);
                            JOptionPane.showMessageDialog(null, "Los datos fueron modificados con exito.");
                            fueModificado = true;
                            save();
                        }
                    }
                }
            }
        }
    return fueModificado;
    }
    
    public boolean eliminarCliente(Cliente cliente) {
        boolean fueEliminado = true;
        if (cliente.getListadoCitas().size() > 0){
            JOptionPane.showMessageDialog(null, "El cliente tiene citas, no se puede eliminar");
            fueEliminado = false;
        }else{
            listaClientes.remove(cliente);
             save();
        }
        return fueEliminado;
    }
    
    public boolean registrarServicio(String nombre, String descripcion) {
        boolean seRegistro = true;
        if (nombre.isEmpty() || descripcion.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Debe ingresar los datos solicitados para crear el servicio");
            seRegistro = false;
        }
        else {
            for (int i = 0; i < listaServicios.size(); i++){
                if (listaServicios.get(i).getNombre().equalsIgnoreCase(nombre)){
                    JOptionPane.showMessageDialog(null, "Ya existe un servicio con ese nombre");
                    seRegistro = false;
                    break;
                }
            }
            if (seRegistro){
                Servicio nuevoServicio = new Servicio(nombre, descripcion);
                listaServicios.add(nuevoServicio);
                JOptionPane.showMessageDialog(null, "Se registro el servicio con exito.");
                save();
            }
        }
    return seRegistro;
    }
    
    public boolean modificarServicio(int indice, String nombre, String descripcion) {
        boolean fueModificado = true;
        if (indice < 1) {
            JOptionPane.showMessageDialog(null, "No se selecciono ningun servicio para modificar.");
            fueModificado = false;
        }else {
            indice -= 1;
            Servicio servicioModificar = listaServicios.get(indice);  
            for (int i = 0; i < listaServicios.size(); i++){
                if (i != indice){
                    if (listaServicios.get(i).getNombre().equalsIgnoreCase(servicioModificar.getNombre())){
                        JOptionPane.showMessageDialog(null, "Ya hay un servicio con ese nombre");
                        fueModificado = false;
                        break;
                    }
                }
            }
            if (fueModificado){
                for (int i = 0; i < listaClientes.size(); i++){
                    for (int j = 0; j < listaClientes.get(i).getListadoCitas().size(); j++){
                        if (listaClientes.get(i).getListadoCitas().get(j).getTipoServicio().getNombre().equalsIgnoreCase(servicioModificar.getNombre())){
                            JOptionPane.showMessageDialog(null, "Una cita utiliza ese servicio, no se puede modificar");
                            fueModificado = false;   
                        }
                    }
                }
            }
            
            if (fueModificado){
                listaServicios.get(indice).setNombre(nombre);
                listaServicios.get(indice).setDescripcion(descripcion);
                JOptionPane.showMessageDialog(null,"El servicio fue modificado con exito");
                save();
            }
        }
        return fueModificado;
    }
    
    public void removerServicio(int indice) {
        if (indice < 1) {
            JOptionPane.showMessageDialog(null, "No se selecciono ningun servicio.");
        }else {
            indice -= 1;
            Servicio servicioBorrar = listaServicios.get(indice);
            boolean borradoPosible = true;
            for (int i = 0; i < listaClientes.size(); i++){
                for (int j = 0; j < listaClientes.get(i).getListadoCitas().size(); j++){
                    if (listaClientes.get(i).getListadoCitas().get(j).getTipoServicio().getNombre().equalsIgnoreCase(servicioBorrar.getNombre())){
                        borradoPosible = false;
                        break;
                    }
                }
            }
            if (borradoPosible == true){
                JOptionPane.showMessageDialog(null, "Se elimino el servicio '" + listaServicios.get(indice).getNombre() + 
                    "' con exito.");
                listaServicios.remove(indice);
                save();
            }else{
                JOptionPane.showMessageDialog(null, "Hay una cita que tiene asignada ese servicio");
            }
        }
    }
    
    public void crearCita(Cliente cliente, int anio, int mes, int dia, String hora, Servicio servicio){
        if (anio < Calendar.getInstance().get(Calendar.YEAR)){
            JOptionPane.showMessageDialog(null, "Ese anio ya paso");
        }else if (mes < Calendar.getInstance().get(Calendar.MONTH) && anio == Calendar.getInstance().get(Calendar.YEAR)) {
            JOptionPane.showMessageDialog(null, "La fecha seleccionada ya paso.");
        }else {
            Calendar cal = Calendar.getInstance();
            int diaMes = cal.get(Calendar.DAY_OF_MONTH);
            if (mes == Calendar.getInstance().get(Calendar.MONTH) && dia < diaMes) {
                JOptionPane.showMessageDialog(null, "La fecha seleccionada ya paso.");
            }else {
                Calendar horaDia = Calendar.getInstance();
                int horaActual = horaDia.get(Calendar.HOUR_OF_DAY);
                int horaSeleccionada = conseguirHora(hora);
                int diaActual = horaDia.get(Calendar.DAY_OF_MONTH);
                if (horaSeleccionada < horaActual && diaActual == dia) {
                    JOptionPane.showMessageDialog(null, "La hora seleccionada no es valida conforme a la fecha.");
                }
                else {
                    int indice = listaClientes.indexOf(cliente);
                    Cita citaCrear = new Cita(cliente, anio, mes, dia, hora, servicio);
                    boolean noExiste = true;
                    for (int i = 0; i < listaClientes.size(); i++){
                        for (int j = 0; j < listaClientes.get(i).getListadoCitas().size(); j++){
                            if (listaClientes.get(i).verificarCampo(citaCrear) == false){
                                noExiste = false;
                            }
                        }
                    }
                    if (noExiste == true) {
                        listaClientes.get(indice).addCita(citaCrear);
                        JOptionPane.showMessageDialog(null, "La cita quedo registrada.");
                        save();
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "La hora indicada se encuentra ocupada.");
                    }
                }
            }
        }
    }
    
    public void modificarCita(int indiceCita, Cliente cliente, int pAnio, int pMes, int pDia, String pHora, int pIndiceServicio) {
        if (pAnio < Calendar.getInstance().get(Calendar.YEAR)){
            JOptionPane.showMessageDialog(null, "Ese anio ya paso");
        }else if (pMes < Calendar.getInstance().get(Calendar.MONTH) && pAnio == Calendar.getInstance().get(Calendar.YEAR)) {
            JOptionPane.showMessageDialog(null, "La fecha seleccionada ya paso.");
        }else {
            Calendar cal = Calendar.getInstance();
            int diaMes = cal.get(Calendar.DAY_OF_MONTH);
            if (pMes == cal.get(Calendar.MONTH) && pDia < diaMes) {
                JOptionPane.showMessageDialog(null, "La fecha seleccionada ya paso.");
            }else {
                Calendar horaDia = Calendar.getInstance();
                int horaActual = horaDia.get(Calendar.HOUR_OF_DAY);
                int horaCita = conseguirHora(pHora);
                int diaActual = horaDia.get(Calendar.DAY_OF_MONTH);
                if (horaCita < horaActual && diaActual == pDia) {
                    JOptionPane.showMessageDialog(null, "La hora seleccionada no es valida conforme a la fecha.");
                }
                else {
                    Cita citaModificar = new Cita(cliente, pAnio, pMes, pDia, pHora, listaServicios.get(pIndiceServicio));
                    boolean puedeModificar = true;
                    for (int i = 0; i < listaClientes.size(); i++){
                        for (int j = 0; j < listaClientes.get(i).getListadoCitas().size(); j++){
                            if (indiceCita == j){
                                if (citaModificar.getCliente().verificarCampo(citaModificar) == false){
                                   puedeModificar = false;
                                   break;
                                }
                            }
                        }
                    }
                    if (puedeModificar == true){
                        int indiceCliente = listaClientes.indexOf(citaModificar.getCliente());
                        listaClientes.get(indiceCliente).getListadoCitas().set(indiceCita - 1, citaModificar);
                        JOptionPane.showMessageDialog(null, "La cita fue modificada con exito");
                        save();
                    }else{
                        JOptionPane.showMessageDialog(null, "Ya hay una cita para esa fecha");
                    }
                }       
            }
        }
    }
    
    public void eliminarCita(Cliente cliente, int indiceCita){
        if (indiceCita < 0){
            JOptionPane.showMessageDialog(null, "Debe seleccionar una cita");
        }else{
            int indiceCliente = listaClientes.indexOf(cliente);
            listaClientes.get(indiceCliente).getListadoCitas().remove(indiceCita);
            JOptionPane.showMessageDialog(null, "La cita fue eliminada");
            save();
        }
    }
    
    
    public void agregarListaEspera(int indice){
        indice -= 1;
        boolean existe = false;
        for (int i = 0; i < listaEspera.size(); i++) { 
            if (listaClientes.get(indice).getTelefono() == listaEspera.get(i).getTelefono()) {
                existe = true;
                break;
            }
        }
        if (existe == false) {
            listaEspera.add(listaClientes.get(indice));
            JOptionPane.showMessageDialog(null, "El cliente fue ingresado a la lista de espera");
            save();
        }
        else {
            JOptionPane.showMessageDialog(null, "El cliente ya se encuentra en la lista de espera.");
        }
    }
    
    public void eliminarListaEspera(int indice){
        System.out.println(indice);
        System.out.println(listaClientes.get(indice).getNombre());
        boolean encontrado = false;
        int indiceEncontrado = -1;
        for (int i = 0; i < listaEspera.size(); i++){
            if (listaEspera.get(i).getTelefono() == listaClientes.get(indice).getTelefono()){
                encontrado = true;
                indiceEncontrado = i;
                break;
            }
        }
        if (encontrado == true){
            listaEspera.remove(indiceEncontrado);
            JOptionPane.showMessageDialog(null, "El cliente fue eliminado de la lista de espera");
            save();

        }else{
            JOptionPane.showMessageDialog(null, "Ese cliente no esta en la lista de espera");
        }
    }
    
    public void confirmarCita(Cliente cliente, int indice){
        if (indice < 0){
            JOptionPane.showMessageDialog(null, "Debe seleccionar una cita");
        }else{
            int indiceCliente = listaClientes.indexOf(cliente);
            if (listaClientes.get(indiceCliente).getListadoCitas().get(indice).isConfirmado() == true){
                JOptionPane.showMessageDialog(null, "La cita ya habia sido confirmada");
            }else{
                JOptionPane.showMessageDialog(null, "La cita ahora esta confirmada");
                listaClientes.get(indiceCliente).getListadoCitas().get(indice).confirmarCita();
                save();
            }
        }
    }
    
    public int sizeClientes(){
        return listaClientes.size();
    }
    
    public int sizeCitas(){
        int largo = 0;
        for (int i = 0; i < listaClientes.size(); i++){
            largo = largo + listaClientes.get(i).getListadoCitas().size();
        }
        return largo;
    }
    
    public int conseguirHora(String hora) {
        int hora_retornar = 0;
        if (hora.equals("12:00 am")) {
            hora_retornar = 0;
        }else if (hora.equals("1:00 am")) {
            hora_retornar = 1;
        }else if (hora.equals("2:00 am")) {
            hora_retornar = 2;
        }else if (hora.equals("3:00 am")) {
            hora_retornar = 3;
        }else if (hora.equals("4:00 am")) {
            hora_retornar = 4;
        }else if (hora.equals("5:00 am")) {
            hora_retornar = 5;
        }else if (hora.equals("6:00 am")) {
            hora_retornar = 6;
        }else if (hora.equals("7:00 am")) {
            hora_retornar = 7;
        }else if (hora.equals("8:00 am")) {
            hora_retornar = 8;
        }else if (hora.equals("9:00 am")) {
            hora_retornar = 9;
        }else if (hora.equals("10:00 am")) {
            hora_retornar = 10;
        }else if (hora.equals("11:00 am")) {
            hora_retornar = 11;
        }else if (hora.equals("12:00 pm")) {
            hora_retornar = 12;
        }else if (hora.equals("1:00 pm")) {
            hora_retornar = 13;
        }else if (hora.equals("2:00 pm")) {
            hora_retornar = 14;
        }else if (hora.equals("3:00 pm")) {
            hora_retornar = 15;
        }else if (hora.equals("4:00 pm")) {
            hora_retornar = 16;
        }else if (hora.equals("5:00 pm")) {
            hora_retornar = 17;
        }else if (hora.equals("6:00 pm")) {
            hora_retornar = 18;
        }else if (hora.equals("7:00 pm")) {
            hora_retornar = 19;
        }else if (hora.equals("8:00 pm")) {
            hora_retornar = 20;
        }else if (hora.equals("9:00 pm")) {
            hora_retornar = 21;
        }else if (hora.equals("10:00 pm")) {
            hora_retornar = 22;
        }else if (hora.equals("11:00 pm")) {
            hora_retornar = 23;
        }
        return hora_retornar;
    }

    public ArrayList<Cliente> getListaClientes() {
        return listaClientes;
    }

    public ArrayList<Servicio> getListaServicios() {
        return listaServicios;
    }

    public ArrayList<Dia> getListaHorarios() {
        return listaHorarios;
    }

    
    public ArrayList<Cliente> getListaEspera(){
        return listaEspera;
    }
    
    public void save() {
        String  fileName = "clientes.bin";
        //salvar clientes y citas
        try {
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName));
            os.writeObject(listaClientes);
            os.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        
        // Salvar los servicios.
        fileName = "servicios.bin";
        try {
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName));
            os.writeObject(listaServicios);
            os.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        
        //Salvar listaEspera
        fileName = "listaEspera.bin";
        try {
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName));
            os.writeObject(listaEspera);
            os.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        // Salvar los horarios.
        fileName = "horarios.bin";
        try {
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName));
            os.writeObject(listaHorarios);
            os.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < listaHorarios.size(); i++) {
            System.out.println("Dia " + (i + 1));
        }
    }
    
    public boolean enviarCorreo(Correo c){
        try{
            Properties p = new Properties();
            p.put("mail.smtp.host", "smtp.gmail.com");
            p.setProperty("mail.smtp.starttls.enable", "true");
            p.setProperty("mail.smtp.port", "587");
            p.setProperty("mail.smtp.user", c.getUsuarioCorreo());
            p.setProperty("mail.smtp.auth", "true");
            
            Session s= Session.getDefaultInstance(p,null);
            
            BodyPart texto = new MimeBodyPart();
            texto.setText(c.getMensaje());
            BodyPart adjunto = new MimeBodyPart();
            
            
            if(!c.getRutaArchivo().equals("")){
                adjunto.setDataHandler(new DataHandler(new FileDataSource(c.getRutaArchivo())));
                adjunto.setFileName(c.getNombreArchivo());
            }
            MimeMultipart m = new MimeMultipart();
            m.addBodyPart(texto);
            
            
            if(!c.getRutaArchivo().equals("")){
                m.addBodyPart(adjunto);
            }
            
            MimeMessage mensaje = new MimeMessage(s);
            mensaje.setFrom(new InternetAddress(c.getUsuarioCorreo()));
            mensaje.addRecipient(Message.RecipientType.TO, new InternetAddress(c.getDestino()));
            mensaje.setSubject(c.getAsunto());
            mensaje.setContent(m);
            
            
            
            Transport t = s.getTransport("smtp");
            t.connect(c.getUsuarioCorreo(), c.getContrasenia());
            
            t.sendMessage(mensaje, mensaje.getAllRecipients());
            t.close();
            return true;
            
        }catch(Exception e){
            System.out.println("Error" + e);
            return false;
        }
        
    }

}
