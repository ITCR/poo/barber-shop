/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaz;

import Controlador.Control_Barberia;
import Controlador.StringInt;
import Objetos.Cliente;
import Objetos.Servicio;
import Objetos.Correo;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Calendar;
import java.util.Locale;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 *
 * @author Charlie
 */
public class Ventana extends javax.swing.JFrame {

    Control_Barberia sistema = new Control_Barberia();
    Correo c = new Correo();
    
    public Ventana() {
        initComponents();
    }
    
    
    
    public void VentanaCita(int dia, int mes, int anio) {
        mes -= 1;
        boolean noHayCitas = true;
        String texto = "Lista Citas:   " + dia + " | " + mes + " | " + anio + "\n\n";
        for (int i = 0; i < sistema.getListaClientes().size(); i++) {
            for (int j = 0; j < sistema.getListaClientes().get(i).getListadoCitas().size(); j++){
                int diaCiclo = sistema.getListaClientes().get(i).getListadoCitas().get(j).getDia();
                int mesCiclo = sistema.getListaClientes().get(i).getListadoCitas().get(j).getMes();
                int anioCiclo = sistema.getListaClientes().get(i).getListadoCitas().get(j).getAnio();
                if (diaCiclo == dia && mesCiclo == mes && anioCiclo == anio) {
                    String nombre = sistema.getListaClientes().get(i).getListadoCitas().get(j).getCliente().getNombre();
                    String apellido = sistema.getListaClientes().get(i).getListadoCitas().get(j).getCliente().getApellido1();
                    String servicio = sistema.getListaClientes().get(i).getListadoCitas().get(j).getTipoServicio().getNombre();
                    texto += (nombre + " " + apellido + " - " + servicio + 
                        " " + sistema.getListaClientes().get(i).getListadoCitas().get(j).getHora() + "\n");
                    noHayCitas = false;
                }
            }
        }
        if (noHayCitas == false) {
            JOptionPane.showMessageDialog(null, texto);
        }
        else {
            texto += "No hay citas registradas para esta fecha.";
            JOptionPane.showMessageDialog(null, texto);
        }
    }
    
    public void actualizarCalendarioMes() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yyyy");
        int dia = 1;
        int mes = cb_mesCalendario.getSelectedIndex();
        cb_mesSemanal.setSelectedIndex(mes);
        //cb_semanaSemanal.setSelectedIndex(0);
        if (mes < 1) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String year = tf_anioCalendario.getText();
        StringInt numero = new StringInt(year);
        int anio = numero.pasar_a_Int();
        YearMonth yearMonthObject = YearMonth.of(anio, mes);
        int daysInMonth = yearMonthObject.lengthOfMonth();
        LocalDate date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
        String diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia0.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia1.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia2.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia3.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia4.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia5.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia6.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia7.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia8.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia9.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia10.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia11.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia12.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia13.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia14.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia15.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia16.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia17.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia18.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia19.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia20.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia21.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia22.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia23.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia24.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia25.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia26.setText(diaFinal + " " + dia);
        dia += 1;
        date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
        diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
        if (diaFinal.equals("Mon")) {
            diaFinal = "Lunes";
        }
        if (diaFinal.equals("Tue")) {
            diaFinal = "Martes";
        }
        if (diaFinal.equals("Wed")) {
            diaFinal = "Miercoles";
        }
        if (diaFinal.equals("Thu")) {
            diaFinal = "Jueves";
        }
        if (diaFinal.equals("Fri")) {
            diaFinal = "Viernes";
        }
        if (diaFinal.equals("Sat")) {
            diaFinal = "Sabado";
        }
        if (diaFinal.equals("Sun")) {
            diaFinal = "Domingo";
        }
        dia27.setText(diaFinal + " " + dia);
        dia += 1;
        if (dia <= daysInMonth) {
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia28.setText(diaFinal + " " + dia);
            dia28.setEnabled(true);
        }
        else {
            dia28.setText("");
            dia28.setEnabled(false);
        }
        dia += 1;
        if (dia <= daysInMonth) {
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia29.setText(diaFinal + " " + dia);
            dia29.setEnabled(true);
        }
        else {
            dia29.setText("");
            dia29.setEnabled(false);
        }
        dia += 1;
        if (dia <= daysInMonth) {
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia30.setText(diaFinal + " " + dia);
            dia30.setEnabled(true);
        }
        else {
            dia30.setText("");
            dia30.setEnabled(false);
        }
    }
    
    public void actualizarCalendarioSemana() {
        int indice = cb_semanaSemanal.getSelectedIndex();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yyyy");
        String year = tf_anioCalendario.getText();
        StringInt numero = new StringInt(year);
        int anio = numero.pasar_a_Int();
        int mes = cb_mesCalendario.getSelectedIndex();
            cb_mesSemanal.setSelectedIndex(mes);
            if (mes < 1) {
                mes = 1;
            }
            else {
                mes += 1;
            }
        YearMonth yearMonthObject = YearMonth.of(anio, mes);
        int daysInMonth = yearMonthObject.lengthOfMonth();
        if (indice < 1) { // Semana 1
            System.out.println("Entre A");
            int dia = 1;
            LocalDate date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            String diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia0Semana.setText(diaFinal + " " + dia);
            dia0Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia1Semana.setText(diaFinal + " " + dia);
            dia1Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia2Semana.setText(diaFinal + " " + dia);
            dia2Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia3Semana.setText(diaFinal + " " + dia);
            dia3Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia4Semana.setText(diaFinal + " " + dia);
            dia4Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia5Semana.setText(diaFinal + " " + dia);
            dia5Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia6Semana.setText(diaFinal + " " + dia);
            dia6Semana.setEnabled(true);
        }
        if (indice == 1) { // Semana 2
            System.out.println("Entre B");
            int dia = 8;
            LocalDate date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            String diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia0Semana.setText(diaFinal + " " + dia);
            dia0Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia1Semana.setText(diaFinal + " " + dia);
            dia1Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia2Semana.setText(diaFinal + " " + dia);
            dia2Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia3Semana.setText(diaFinal + " " + dia);
            dia3Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia4Semana.setText(diaFinal + " " + dia);
            dia4Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia5Semana.setText(diaFinal + " " + dia);
            dia5Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia6Semana.setText(diaFinal + " " + dia);
            dia6Semana.setEnabled(true);
        }
        if (indice == 2 ) { // Semana 3
            int dia = 15;
            LocalDate date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            String diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia0Semana.setText(diaFinal + " " + dia);
            dia0Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia1Semana.setText(diaFinal + " " + dia);
            dia1Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia2Semana.setText(diaFinal + " " + dia);
            dia2Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia3Semana.setText(diaFinal + " " + dia);
            dia3Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia4Semana.setText(diaFinal + " " + dia);
            dia4Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia5Semana.setText(diaFinal + " " + dia);
            dia5Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia6Semana.setText(diaFinal + " " + dia);
            dia6Semana.setEnabled(true);
        }
        if (indice == 3) { // Semana 4
            int dia = 22;
            LocalDate date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            String diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia0Semana.setText(diaFinal + " " + dia);
            dia0Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia1Semana.setText(diaFinal + " " + dia);
            dia1Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia2Semana.setText(diaFinal + " " + dia);
            dia2Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia3Semana.setText(diaFinal + " " + dia);
            dia3Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia4Semana.setText(diaFinal + " " + dia);
            dia4Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia5Semana.setText(diaFinal + " " + dia);
            dia5Semana.setEnabled(true);
            dia += 1;
            date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            if (diaFinal.equals("Mon")) {
                diaFinal = "Lunes";
            }
            if (diaFinal.equals("Tue")) {
                diaFinal = "Martes";
            }
            if (diaFinal.equals("Wed")) {
                diaFinal = "Miercoles";
            }
            if (diaFinal.equals("Thu")) {
                diaFinal = "Jueves";
            }
            if (diaFinal.equals("Fri")) {
                diaFinal = "Viernes";
            }
            if (diaFinal.equals("Sat")) {
                diaFinal = "Sabado";
            }
            if (diaFinal.equals("Sun")) {
                diaFinal = "Domingo";
            }
            dia6Semana.setText(diaFinal + " " + dia);
            dia6Semana.setEnabled(true);
        }
        if (indice == 4) { // Semana 5
            int dia = 29;
            if (dia <= daysInMonth) {
                LocalDate date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
                String diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
                if (diaFinal.equals("Mon")) {
                    diaFinal = "Lunes";
                }
                if (diaFinal.equals("Tue")) {
                    diaFinal = "Martes";
                }
                if (diaFinal.equals("Wed")) {
                    diaFinal = "Miercoles";
                }
                if (diaFinal.equals("Thu")) {
                    diaFinal = "Jueves";
                }
                if (diaFinal.equals("Fri")) {
                    diaFinal = "Viernes";
                }
                if (diaFinal.equals("Sat")) {
                    diaFinal = "Sabado";
                }
                if (diaFinal.equals("Sun")) {
                    diaFinal = "Domingo";
                }
                dia0Semana.setText(diaFinal + " " + dia);
                dia0Semana.setEnabled(true);
            }
            else {
                dia0Semana.setText("");
                dia0Semana.setEnabled(false);
            }
            dia += 1;
            if (dia <= daysInMonth) {
                LocalDate date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
                String diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
                if (diaFinal.equals("Mon")) {
                    diaFinal = "Lunes";
                }
                if (diaFinal.equals("Tue")) {
                    diaFinal = "Martes";
                }
                if (diaFinal.equals("Wed")) {
                    diaFinal = "Miercoles";
                }
                if (diaFinal.equals("Thu")) {
                    diaFinal = "Jueves";
                }
                if (diaFinal.equals("Fri")) {
                    diaFinal = "Viernes";
                }
                if (diaFinal.equals("Sat")) {
                    diaFinal = "Sabado";
                }
                if (diaFinal.equals("Sun")) {
                    diaFinal = "Domingo";
                }
                dia1Semana.setText(diaFinal + " " + dia);
                dia1Semana.setEnabled(true);
            }
            else {
                dia1Semana.setText("");
                dia1Semana.setEnabled(false);
            }
            dia += 1;
            if (dia <= daysInMonth) {
                LocalDate date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter);
                String diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
                if (diaFinal.equals("Mon")) {
                    diaFinal = "Lunes";
                }
                if (diaFinal.equals("Tue")) {
                    diaFinal = "Martes";
                }
                if (diaFinal.equals("Wed")) {
                    diaFinal = "Miercoles";
                }
                if (diaFinal.equals("Thu")) {
                    diaFinal = "Jueves";
                }
                if (diaFinal.equals("Fri")) {
                    diaFinal = "Viernes";
                }
                if (diaFinal.equals("Sat")) {
                    diaFinal = "Sabado";
                }
                if (diaFinal.equals("Sun")) {
                    diaFinal = "Domingo";
                }
                dia2Semana.setText(diaFinal + " " + dia);
                dia2Semana.setEnabled(true);
            }
            else {
                dia2Semana.setText("");
                dia2Semana.setEnabled(false);
            }
            dia3Semana.setText("");
            dia3Semana.setEnabled(false);
            dia4Semana.setText("");
            dia4Semana.setEnabled(false);
            dia5Semana.setText("");
            dia5Semana.setEnabled(false);
            dia6Semana.setText("");
            dia6Semana.setEnabled(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        Panel = new javax.swing.JPanel();
        panelPrincipal = new javax.swing.JPanel();
        jLabel51 = new javax.swing.JLabel();
        panelRegistrarCliente = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        tf_registrarNombreCliente = new javax.swing.JTextField();
        tf_registrarApellido1Cliente = new javax.swing.JTextField();
        tf_registrarApellido2Cliente = new javax.swing.JTextField();
        tf_registrarCorreoCliente = new javax.swing.JTextField();
        tf_registrarTelefonoCliente = new javax.swing.JTextField();
        btn_registrarCliente = new javax.swing.JButton();
        btn_cancelarRegistrarCliente = new javax.swing.JButton();
        panelModificarCliente = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        tf_modificarNombreCliente = new javax.swing.JTextField();
        tf_modificarApellido1Cliente = new javax.swing.JTextField();
        tf_modificarApellido2Cliente = new javax.swing.JTextField();
        tf_modificarCorreoCliente = new javax.swing.JTextField();
        tf_modificarTelefonoCliente = new javax.swing.JTextField();
        cb_modificarCliente = new javax.swing.JComboBox<>();
        btn_modificarCliente = new javax.swing.JButton();
        btn_cancelaModificarCliente = new javax.swing.JButton();
        panelEliminarCliente = new javax.swing.JPanel();
        btn_cancelarEliminarCliente = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        tf_eliminarNombreCliente = new javax.swing.JTextField();
        tf_eliminarApellido1Cliente = new javax.swing.JTextField();
        tf_eliminarApellido2Cliente = new javax.swing.JTextField();
        tf_eliminarCorreoCliente = new javax.swing.JTextField();
        tf_eliminarTelefonoCliente = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        cb_eliminarCliente = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        btn_eliminarCliente = new javax.swing.JButton();
        panelConsultarCliente = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        cb_consultarCliente = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        btn_cancelarConsultarCliente = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        tf_consultarClienteNombre = new javax.swing.JTextField();
        tf_consultarClienteApellido1 = new javax.swing.JTextField();
        tf_consultarClienteApellido2 = new javax.swing.JTextField();
        tf_consultarClienteCorreo = new javax.swing.JTextField();
        tf_consultarClienteTelefono = new javax.swing.JTextField();
        panelCrearCita = new javax.swing.JPanel();
        cb_clienteRegistrarCita = new javax.swing.JComboBox<>();
        jLabel21 = new javax.swing.JLabel();
        cb_anioRegistrarCita = new javax.swing.JComboBox<>();
        jLabel22 = new javax.swing.JLabel();
        cb_mesRegistrarCita = new javax.swing.JComboBox<>();
        cb_diaRegistrarCita = new javax.swing.JComboBox<>();
        jLabel23 = new javax.swing.JLabel();
        cb_horasRegistrarCita = new javax.swing.JComboBox<>();
        jLabel24 = new javax.swing.JLabel();
        cb_servicioRegistrarCita = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        ta_servicioRegistrarCita = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        panelManejoServicios = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        cb_manejarServicios = new javax.swing.JComboBox<>();
        jLabel19 = new javax.swing.JLabel();
        tf_nombreServicio = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ta_descripcionServicio = new javax.swing.JTextArea();
        btn_agregarServicio = new javax.swing.JButton();
        btn_modificarServicio = new javax.swing.JButton();
        btn_eliminarServicio = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        panelHorario = new javax.swing.JPanel();
        jLabel38 = new javax.swing.JLabel();
        cb_horarioDia = new javax.swing.JComboBox();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        cb_HorarioInicio = new javax.swing.JComboBox();
        cb_HorarioFin = new javax.swing.JComboBox();
        btn_guardarHorario = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        panelCalendario = new javax.swing.JPanel();
        menuPanelCambio = new javax.swing.JTabbedPane();
        vistaMensual = new javax.swing.JPanel();
        dia0 = new javax.swing.JButton();
        dia1 = new javax.swing.JButton();
        dia2 = new javax.swing.JButton();
        dia3 = new javax.swing.JButton();
        dia4 = new javax.swing.JButton();
        dia5 = new javax.swing.JButton();
        dia6 = new javax.swing.JButton();
        dia7 = new javax.swing.JButton();
        dia8 = new javax.swing.JButton();
        dia9 = new javax.swing.JButton();
        dia10 = new javax.swing.JButton();
        dia11 = new javax.swing.JButton();
        dia12 = new javax.swing.JButton();
        dia13 = new javax.swing.JButton();
        dia14 = new javax.swing.JButton();
        dia15 = new javax.swing.JButton();
        dia16 = new javax.swing.JButton();
        dia17 = new javax.swing.JButton();
        dia18 = new javax.swing.JButton();
        dia19 = new javax.swing.JButton();
        dia20 = new javax.swing.JButton();
        dia21 = new javax.swing.JButton();
        dia22 = new javax.swing.JButton();
        dia23 = new javax.swing.JButton();
        dia24 = new javax.swing.JButton();
        dia25 = new javax.swing.JButton();
        dia26 = new javax.swing.JButton();
        dia27 = new javax.swing.JButton();
        dia28 = new javax.swing.JButton();
        dia29 = new javax.swing.JButton();
        dia30 = new javax.swing.JButton();
        jButton40 = new javax.swing.JButton();
        jLabel26 = new javax.swing.JLabel();
        tf_anioCalendario = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        cb_mesCalendario = new javax.swing.JComboBox<>();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(10, 0), new java.awt.Dimension(10, 0), new java.awt.Dimension(10, 32767));
        vistaSemanal = new javax.swing.JPanel();
        dia0Semana = new javax.swing.JButton();
        dia1Semana = new javax.swing.JButton();
        dia2Semana = new javax.swing.JButton();
        dia3Semana = new javax.swing.JButton();
        dia4Semana = new javax.swing.JButton();
        dia5Semana = new javax.swing.JButton();
        dia6Semana = new javax.swing.JButton();
        cb_semanaSemanal = new javax.swing.JComboBox<>();
        cb_mesSemanal = new javax.swing.JComboBox<>();
        jLabel27 = new javax.swing.JLabel();
        tf_anioCalendario1 = new javax.swing.JTextField();
        jButton17 = new javax.swing.JButton();
        jButton18 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        panelConsultarServicio = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        cb_servicioConsulta = new javax.swing.JComboBox<>();
        tf_nombreServicioConsulta = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        ta_descripcionServicioConsulta = new javax.swing.JTextArea();
        jButton11 = new javax.swing.JButton();
        jLabel41 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        panelModificarCita = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        cb_citasModificarCita = new javax.swing.JComboBox<>();
        jLabel33 = new javax.swing.JLabel();
        cb_anioModificarCita = new javax.swing.JComboBox<>();
        cb_mesModificarCita = new javax.swing.JComboBox<>();
        cb_diaModificarCita = new javax.swing.JComboBox<>();
        jLabel34 = new javax.swing.JLabel();
        cb_serviciosModificarCita = new javax.swing.JComboBox<>();
        jScrollPane4 = new javax.swing.JScrollPane();
        ta_descripcionModificarCita = new javax.swing.JTextArea();
        jLabel35 = new javax.swing.JLabel();
        cb_horaModificarCita = new javax.swing.JComboBox<>();
        btn_modificarCita = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();
        jLabel44 = new javax.swing.JLabel();
        cb_clienteModificarCita = new javax.swing.JComboBox();
        panelConfirmarCita = new javax.swing.JPanel();
        jLabel39 = new javax.swing.JLabel();
        cb_confirmarCita = new javax.swing.JComboBox();
        jButton6 = new javax.swing.JButton();
        jButton15 = new javax.swing.JButton();
        jLabel45 = new javax.swing.JLabel();
        cb_clienteConfirmar = new javax.swing.JComboBox();
        panelListaEspera = new javax.swing.JPanel();
        jLabel36 = new javax.swing.JLabel();
        cb_listaEspera = new javax.swing.JComboBox();
        tbn_agrgearListaEspera = new javax.swing.JButton();
        btn_cancelarListaEspera = new javax.swing.JButton();
        btn_eliminarListaEspera = new javax.swing.JButton();
        jLabel50 = new javax.swing.JLabel();
        listaEspera = new javax.swing.JComboBox<>();
        panelConsultarListaEspera = new javax.swing.JPanel();
        jLabel37 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        ta_clienteListaEspera = new javax.swing.JTextArea();
        jButton2 = new javax.swing.JButton();
        panelEliminarCita = new javax.swing.JPanel();
        jLabel40 = new javax.swing.JLabel();
        cb_eliminarCitaCliente = new javax.swing.JComboBox();
        btn_eliminarCita = new javax.swing.JButton();
        jButton19 = new javax.swing.JButton();
        jLabel43 = new javax.swing.JLabel();
        cb_citaEliminarCita = new javax.swing.JComboBox();
        panelCorreo = new javax.swing.JPanel();
        jLabel46 = new javax.swing.JLabel();
        cb_correo = new javax.swing.JComboBox();
        jLabel47 = new javax.swing.JLabel();
        tf_CorreoAsunto = new javax.swing.JTextField();
        tf_Correocorreo = new javax.swing.JTextField();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        ta_CorreoDescripcion = new javax.swing.JTextArea();
        btn_mandarCorreo = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        barraOperaciones = new javax.swing.JMenuBar();
        menuClientes = new javax.swing.JMenu();
        registrarCliente = new javax.swing.JMenuItem();
        modificarCliente = new javax.swing.JMenuItem();
        eliminarCliente = new javax.swing.JMenuItem();
        separadorCliente = new javax.swing.JPopupMenu.Separator();
        consultarCliente = new javax.swing.JMenuItem();
        menuCitas = new javax.swing.JMenu();
        crearCita = new javax.swing.JMenuItem();
        modificarCita = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        confirmarCita = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        calendario = new javax.swing.JMenuItem();
        menuServicios = new javax.swing.JMenu();
        manejarServicios = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        consultarServicio = new javax.swing.JMenuItem();
        menuAdministracion = new javax.swing.JMenu();
        horarioSistema = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMenuItem4 = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Barberia: El Computin");
        setResizable(false);

        Panel.setLayout(new java.awt.CardLayout());

        jLabel51.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fondoPrincipal_1.png"))); // NOI18N

        javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
        panelPrincipal.setLayout(panelPrincipalLayout);
        panelPrincipalLayout.setHorizontalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel51, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        panelPrincipalLayout.setVerticalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel51, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        Panel.add(panelPrincipal, "card2");

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Nombre:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Primer Apellido:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Segundo Apellido:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Correo:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Telefono:");

        tf_registrarNombreCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_registrarApellido1Cliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_registrarApellido2Cliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_registrarCorreoCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_registrarTelefonoCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        btn_registrarCliente.setText("Registrar");
        btn_registrarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_registrarClienteActionPerformed(evt);
            }
        });

        btn_cancelarRegistrarCliente.setText("Cancelar");
        btn_cancelarRegistrarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarRegistrarClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelRegistrarClienteLayout = new javax.swing.GroupLayout(panelRegistrarCliente);
        panelRegistrarCliente.setLayout(panelRegistrarClienteLayout);
        panelRegistrarClienteLayout.setHorizontalGroup(
            panelRegistrarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRegistrarClienteLayout.createSequentialGroup()
                .addGap(200, 200, 200)
                .addGroup(panelRegistrarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelRegistrarClienteLayout.createSequentialGroup()
                        .addGap(95, 95, 95)
                        .addComponent(btn_registrarCliente)
                        .addGap(97, 97, 97)
                        .addComponent(btn_cancelarRegistrarCliente))
                    .addGroup(panelRegistrarClienteLayout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(tf_registrarNombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelRegistrarClienteLayout.createSequentialGroup()
                        .addGroup(panelRegistrarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelRegistrarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tf_registrarApellido1Cliente)
                            .addComponent(tf_registrarApellido2Cliente)
                            .addComponent(tf_registrarCorreoCliente)
                            .addComponent(tf_registrarTelefonoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(259, Short.MAX_VALUE))
        );
        panelRegistrarClienteLayout.setVerticalGroup(
            panelRegistrarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelRegistrarClienteLayout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addGroup(panelRegistrarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tf_registrarNombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(panelRegistrarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tf_registrarApellido1Cliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(panelRegistrarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(tf_registrarApellido2Cliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(panelRegistrarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(tf_registrarCorreoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(panelRegistrarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(tf_registrarTelefonoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(64, 64, 64)
                .addGroup(panelRegistrarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_registrarCliente)
                    .addComponent(btn_cancelarRegistrarCliente))
                .addContainerGap(79, Short.MAX_VALUE))
        );

        Panel.add(panelRegistrarCliente, "card3");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Seleccione el Cliente:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Nombre Completo:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setText("Correo:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Telefono:");

        tf_modificarNombreCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_modificarApellido1Cliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_modificarApellido2Cliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_modificarCorreoCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_modificarTelefonoCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        cb_modificarCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cb_modificarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_modificarClienteActionPerformed(evt);
            }
        });

        btn_modificarCliente.setText("Guardar Cambios");
        btn_modificarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_modificarClienteActionPerformed(evt);
            }
        });

        btn_cancelaModificarCliente.setText("Cancelar");
        btn_cancelaModificarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelaModificarClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelModificarClienteLayout = new javax.swing.GroupLayout(panelModificarCliente);
        panelModificarCliente.setLayout(panelModificarClienteLayout);
        panelModificarClienteLayout.setHorizontalGroup(
            panelModificarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModificarClienteLayout.createSequentialGroup()
                .addGap(131, 131, 131)
                .addGroup(panelModificarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel7)
                    .addComponent(jLabel9)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(panelModificarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelModificarClienteLayout.createSequentialGroup()
                        .addComponent(btn_modificarCliente)
                        .addGap(65, 65, 65)
                        .addComponent(btn_cancelaModificarCliente))
                    .addGroup(panelModificarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(cb_modificarCliente, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelModificarClienteLayout.createSequentialGroup()
                            .addComponent(tf_modificarNombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(tf_modificarApellido1Cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(tf_modificarApellido2Cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(tf_modificarCorreoCliente, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(tf_modificarTelefonoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(208, Short.MAX_VALUE))
        );
        panelModificarClienteLayout.setVerticalGroup(
            panelModificarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelModificarClienteLayout.createSequentialGroup()
                .addContainerGap(138, Short.MAX_VALUE)
                .addGroup(panelModificarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cb_modificarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(panelModificarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(tf_modificarNombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_modificarApellido1Cliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_modificarApellido2Cliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(panelModificarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(tf_modificarCorreoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(panelModificarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(tf_modificarTelefonoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(panelModificarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_modificarCliente)
                    .addComponent(btn_cancelaModificarCliente))
                .addGap(79, 79, 79))
        );

        Panel.add(panelModificarCliente, "card4");

        btn_cancelarEliminarCliente.setText("Cancelar");
        btn_cancelarEliminarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarEliminarClienteActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setText("Correo:");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel11.setText("Telefono:");

        tf_eliminarNombreCliente.setEditable(false);
        tf_eliminarNombreCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_eliminarApellido1Cliente.setEditable(false);
        tf_eliminarApellido1Cliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_eliminarApellido2Cliente.setEditable(false);
        tf_eliminarApellido2Cliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_eliminarCorreoCliente.setEditable(false);
        tf_eliminarCorreoCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_eliminarTelefonoCliente.setEditable(false);
        tf_eliminarTelefonoCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel12.setText("Seleccione el Cliente:");

        cb_eliminarCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cb_eliminarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_eliminarClienteActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel13.setText("Nombre Completo:");

        btn_eliminarCliente.setText("Eliminar Cliente");
        btn_eliminarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_eliminarClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelEliminarClienteLayout = new javax.swing.GroupLayout(panelEliminarCliente);
        panelEliminarCliente.setLayout(panelEliminarClienteLayout);
        panelEliminarClienteLayout.setHorizontalGroup(
            panelEliminarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEliminarClienteLayout.createSequentialGroup()
                .addGap(126, 126, 126)
                .addGroup(panelEliminarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel10)
                    .addComponent(jLabel13)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12))
                .addGap(18, 18, 18)
                .addGroup(panelEliminarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelEliminarClienteLayout.createSequentialGroup()
                        .addComponent(btn_eliminarCliente)
                        .addGap(65, 65, 65)
                        .addComponent(btn_cancelarEliminarCliente))
                    .addGroup(panelEliminarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(cb_eliminarCliente, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelEliminarClienteLayout.createSequentialGroup()
                            .addComponent(tf_eliminarNombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(tf_eliminarApellido1Cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(tf_eliminarApellido2Cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(tf_eliminarCorreoCliente, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(tf_eliminarTelefonoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(213, Short.MAX_VALUE))
        );
        panelEliminarClienteLayout.setVerticalGroup(
            panelEliminarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelEliminarClienteLayout.createSequentialGroup()
                .addContainerGap(138, Short.MAX_VALUE)
                .addGroup(panelEliminarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(cb_eliminarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(panelEliminarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(tf_eliminarNombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_eliminarApellido1Cliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_eliminarApellido2Cliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(panelEliminarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(tf_eliminarCorreoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(panelEliminarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(tf_eliminarTelefonoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(panelEliminarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_eliminarCliente)
                    .addComponent(btn_cancelarEliminarCliente))
                .addGap(79, 79, 79))
        );

        Panel.add(panelEliminarCliente, "card5");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel14.setText("Seleccione el Cliente:");

        cb_consultarCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cb_consultarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_consultarClienteActionPerformed(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel15.setText("Nombre Completo:");

        btn_cancelarConsultarCliente.setText("Regresar");
        btn_cancelarConsultarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarConsultarClienteActionPerformed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel16.setText("Correo:");

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel17.setText("Telefono:");

        tf_consultarClienteNombre.setEditable(false);
        tf_consultarClienteNombre.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_consultarClienteApellido1.setEditable(false);
        tf_consultarClienteApellido1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_consultarClienteApellido2.setEditable(false);
        tf_consultarClienteApellido2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_consultarClienteCorreo.setEditable(false);
        tf_consultarClienteCorreo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        tf_consultarClienteTelefono.setEditable(false);
        tf_consultarClienteTelefono.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout panelConsultarClienteLayout = new javax.swing.GroupLayout(panelConsultarCliente);
        panelConsultarCliente.setLayout(panelConsultarClienteLayout);
        panelConsultarClienteLayout.setHorizontalGroup(
            panelConsultarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelConsultarClienteLayout.createSequentialGroup()
                .addContainerGap(711, Short.MAX_VALUE)
                .addComponent(btn_cancelarConsultarCliente)
                .addGap(33, 33, 33))
            .addGroup(panelConsultarClienteLayout.createSequentialGroup()
                .addGap(148, 148, 148)
                .addGroup(panelConsultarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel16)
                    .addComponent(jLabel15)
                    .addComponent(jLabel17)
                    .addComponent(jLabel14))
                .addGap(18, 18, 18)
                .addGroup(panelConsultarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(cb_consultarCliente, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelConsultarClienteLayout.createSequentialGroup()
                        .addComponent(tf_consultarClienteNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tf_consultarClienteApellido1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tf_consultarClienteApellido2, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(tf_consultarClienteCorreo, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_consultarClienteTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelConsultarClienteLayout.setVerticalGroup(
            panelConsultarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConsultarClienteLayout.createSequentialGroup()
                .addContainerGap(170, Short.MAX_VALUE)
                .addGroup(panelConsultarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(cb_consultarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(panelConsultarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(tf_consultarClienteNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_consultarClienteApellido1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tf_consultarClienteApellido2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(panelConsultarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(tf_consultarClienteCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(panelConsultarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(tf_consultarClienteTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(60, 60, 60)
                .addComponent(btn_cancelarConsultarCliente)
                .addGap(37, 37, 37))
        );

        Panel.add(panelConsultarCliente, "card6");

        cb_clienteRegistrarCita.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cb_clienteRegistrarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_clienteRegistrarCitaActionPerformed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel21.setText("Fecha:");

        cb_anioRegistrarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_anioRegistrarCitaActionPerformed(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel22.setText("Seleccione el Cliente:");

        cb_mesRegistrarCita.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));
        cb_mesRegistrarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_mesRegistrarCitaActionPerformed(evt);
            }
        });

        cb_diaRegistrarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_diaRegistrarCitaActionPerformed(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel23.setText("Hora:");

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel24.setText("Tipo de Servicio:");

        cb_servicioRegistrarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_servicioRegistrarCitaActionPerformed(evt);
            }
        });

        ta_servicioRegistrarCita.setEditable(false);
        ta_servicioRegistrarCita.setColumns(20);
        ta_servicioRegistrarCita.setLineWrap(true);
        ta_servicioRegistrarCita.setRows(5);
        jScrollPane2.setViewportView(ta_servicioRegistrarCita);

        jButton1.setText("Registrar Cita");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton5.setText("Cancelar");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelCrearCitaLayout = new javax.swing.GroupLayout(panelCrearCita);
        panelCrearCita.setLayout(panelCrearCitaLayout);
        panelCrearCitaLayout.setHorizontalGroup(
            panelCrearCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCrearCitaLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(panelCrearCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel23)
                    .addComponent(jLabel21))
                .addGap(18, 18, 18)
                .addGroup(panelCrearCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCrearCitaLayout.createSequentialGroup()
                        .addGroup(panelCrearCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelCrearCitaLayout.createSequentialGroup()
                                .addComponent(cb_anioRegistrarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cb_mesRegistrarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(cb_horasRegistrarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cb_diaRegistrarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(65, 65, 65)
                        .addComponent(jLabel24))
                    .addGroup(panelCrearCitaLayout.createSequentialGroup()
                        .addGroup(panelCrearCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(panelCrearCitaLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(cb_servicioRegistrarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelCrearCitaLayout.createSequentialGroup()
                                .addGap(267, 267, 267)
                                .addGroup(panelCrearCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 406, Short.MAX_VALUE)
                                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(65, 65, 65))))
            .addGroup(panelCrearCitaLayout.createSequentialGroup()
                .addGap(97, 97, 97)
                .addComponent(jLabel22)
                .addGap(18, 18, 18)
                .addComponent(cb_clienteRegistrarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelCrearCitaLayout.setVerticalGroup(
            panelCrearCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCrearCitaLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(panelCrearCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(cb_clienteRegistrarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34)
                .addGroup(panelCrearCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(cb_anioRegistrarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_mesRegistrarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_diaRegistrarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24))
                .addGap(18, 18, 18)
                .addComponent(cb_servicioRegistrarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(panelCrearCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCrearCitaLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1)
                        .addGap(18, 18, 18)
                        .addComponent(jButton5))
                    .addGroup(panelCrearCitaLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(panelCrearCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel23)
                            .addComponent(cb_horasRegistrarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(86, Short.MAX_VALUE))
        );

        Panel.add(panelCrearCita, "card7");

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel18.setText("Servicios Existentes: ");

        cb_manejarServicios.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cb_manejarServicios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_manejarServiciosActionPerformed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel19.setText("Servicio:");

        tf_nombreServicio.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel20.setText("Descripcion:");

        ta_descripcionServicio.setColumns(20);
        ta_descripcionServicio.setLineWrap(true);
        ta_descripcionServicio.setRows(5);
        jScrollPane1.setViewportView(ta_descripcionServicio);

        btn_agregarServicio.setText("Agregar Nuevo Servicio");
        btn_agregarServicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_agregarServicioActionPerformed(evt);
            }
        });

        btn_modificarServicio.setText("Guardar Cambio de Servicio");
        btn_modificarServicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_modificarServicioActionPerformed(evt);
            }
        });

        btn_eliminarServicio.setText("Eliminar Servicio del Sistema");
        btn_eliminarServicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_eliminarServicioActionPerformed(evt);
            }
        });

        jButton4.setText("Regresar");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelManejoServiciosLayout = new javax.swing.GroupLayout(panelManejoServicios);
        panelManejoServicios.setLayout(panelManejoServiciosLayout);
        panelManejoServiciosLayout.setHorizontalGroup(
            panelManejoServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelManejoServiciosLayout.createSequentialGroup()
                .addGap(0, 706, Short.MAX_VALUE)
                .addComponent(jButton4)
                .addGap(38, 38, 38))
            .addGroup(panelManejoServiciosLayout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addGroup(panelManejoServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel20)
                    .addComponent(jLabel19)
                    .addComponent(jLabel18))
                .addGap(18, 18, 18)
                .addGroup(panelManejoServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tf_nombreServicio)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
                    .addComponent(cb_manejarServicios, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(panelManejoServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_modificarServicio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_agregarServicio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_eliminarServicio))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelManejoServiciosLayout.setVerticalGroup(
            panelManejoServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelManejoServiciosLayout.createSequentialGroup()
                .addContainerGap(149, Short.MAX_VALUE)
                .addGroup(panelManejoServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(cb_manejarServicios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addGroup(panelManejoServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tf_nombreServicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)
                    .addComponent(btn_agregarServicio))
                .addGap(40, 40, 40)
                .addGroup(panelManejoServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelManejoServiciosLayout.createSequentialGroup()
                        .addGroup(panelManejoServiciosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel20)
                            .addGroup(panelManejoServiciosLayout.createSequentialGroup()
                                .addComponent(btn_modificarServicio)
                                .addGap(40, 40, 40)
                                .addComponent(btn_eliminarServicio)))
                        .addGap(8, 8, 8))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(64, 64, 64)
                .addComponent(jButton4)
                .addGap(28, 28, 28))
        );

        Panel.add(panelManejoServicios, "card8");

        jLabel38.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        jLabel38.setText("Seleccione el dia deseado:");

        cb_horarioDia.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cb_horarioDia.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo" }));
        cb_horarioDia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_horarioDiaActionPerformed(evt);
            }
        });

        jLabel31.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel31.setText("Se abre a las:");

        jLabel32.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel32.setText("Se cierra a las:");

        cb_HorarioInicio.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cb_HorarioInicio.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "12:00 am", "1:00 am", "2:00 am", "3:00 am", "4:00 am", "5:00 am", "6:00 am", "7:00 am", "8:00 am", "9:00 am", "10:00 am", "11:00 am", "12:00 pm", "1:00 pm", "2:00 pm", "3:00 pm", "4:00 pm", "5:00 pm", "6:00 pm", "7:00 pm", "8:00 pm", "9:00 pm", "10:00 pm", "11:00 pm" }));
        cb_HorarioInicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_HorarioInicioActionPerformed(evt);
            }
        });

        cb_HorarioFin.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        cb_HorarioFin.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "12:00 am", "1:00 am", "2:00 am", "3:00 am", "4:00 am", "5:00 am", "6:00 am", "7:00 am", "8:00 am", "9:00 am", "10:00 am", "11:00 am", "12:00 pm", "1:00 pm", "2:00 pm", "3:00 pm", "4:00 pm", "5:00 pm", "6:00 pm", "7:00 pm", "8:00 pm", "9:00 pm", "10:00 pm", "11:00 pm" }));
        cb_HorarioFin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_HorarioFinActionPerformed(evt);
            }
        });

        btn_guardarHorario.setText("Guardar");
        btn_guardarHorario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarHorarioActionPerformed(evt);
            }
        });

        jButton7.setText("Regresar");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelHorarioLayout = new javax.swing.GroupLayout(panelHorario);
        panelHorario.setLayout(panelHorarioLayout);
        panelHorarioLayout.setHorizontalGroup(
            panelHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHorarioLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelHorarioLayout.createSequentialGroup()
                        .addComponent(jButton7)
                        .addGap(51, 51, 51))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelHorarioLayout.createSequentialGroup()
                        .addComponent(btn_guardarHorario)
                        .addGap(358, 358, 358))))
            .addGroup(panelHorarioLayout.createSequentialGroup()
                .addGap(179, 179, 179)
                .addGroup(panelHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel32)
                    .addComponent(jLabel31)
                    .addComponent(jLabel38))
                .addGap(27, 27, 27)
                .addGroup(panelHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cb_horarioDia, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_HorarioInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_HorarioFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 251, Short.MAX_VALUE))
        );
        panelHorarioLayout.setVerticalGroup(
            panelHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelHorarioLayout.createSequentialGroup()
                .addContainerGap(193, Short.MAX_VALUE)
                .addGroup(panelHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel38)
                    .addComponent(cb_horarioDia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(panelHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(cb_HorarioInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(panelHorarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel32)
                    .addComponent(cb_HorarioFin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(69, 69, 69)
                .addComponent(btn_guardarHorario)
                .addGap(56, 56, 56)
                .addComponent(jButton7)
                .addGap(25, 25, 25))
        );

        Panel.add(panelHorario, "card9");

        vistaMensual.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        dia0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia0ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia0, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 23, 120, 40));

        dia1.setMaximumSize(new java.awt.Dimension(120, 40));
        dia1.setMinimumSize(new java.awt.Dimension(120, 40));
        dia1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia1ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia1, new org.netbeans.lib.awtextra.AbsoluteConstraints(144, 23, 120, 40));

        dia2.setMaximumSize(new java.awt.Dimension(120, 40));
        dia2.setMinimumSize(new java.awt.Dimension(120, 40));
        dia2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia2ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia2, new org.netbeans.lib.awtextra.AbsoluteConstraints(273, 23, 120, 40));

        dia3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia3ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia3, new org.netbeans.lib.awtextra.AbsoluteConstraints(408, 23, 120, 40));

        dia4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia4ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia4, new org.netbeans.lib.awtextra.AbsoluteConstraints(537, 23, 120, 40));

        dia5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia5ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia5, new org.netbeans.lib.awtextra.AbsoluteConstraints(666, 23, 120, 40));

        dia6.setMaximumSize(new java.awt.Dimension(120, 40));
        dia6.setMinimumSize(new java.awt.Dimension(120, 40));
        dia6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia6ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia6, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 72, 120, 42));

        dia7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia7ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia7, new org.netbeans.lib.awtextra.AbsoluteConstraints(144, 72, 120, 42));

        dia8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia8ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia8, new org.netbeans.lib.awtextra.AbsoluteConstraints(273, 72, 120, 42));

        dia9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia9ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia9, new org.netbeans.lib.awtextra.AbsoluteConstraints(408, 72, 120, 42));

        dia10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia10ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia10, new org.netbeans.lib.awtextra.AbsoluteConstraints(537, 72, 120, 42));

        dia11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia11ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia11, new org.netbeans.lib.awtextra.AbsoluteConstraints(666, 72, 120, 42));

        dia12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia12ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia12, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 123, 120, 40));

        dia13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia13ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia13, new org.netbeans.lib.awtextra.AbsoluteConstraints(144, 123, 120, 40));

        dia14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia14ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia14, new org.netbeans.lib.awtextra.AbsoluteConstraints(273, 123, 120, 40));

        dia15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia15ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia15, new org.netbeans.lib.awtextra.AbsoluteConstraints(408, 123, 120, 40));

        dia16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia16ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia16, new org.netbeans.lib.awtextra.AbsoluteConstraints(537, 123, 120, 40));

        dia17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia17ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia17, new org.netbeans.lib.awtextra.AbsoluteConstraints(666, 123, 120, 40));

        dia18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia18ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia18, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 172, 120, 40));

        dia19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia19ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia19, new org.netbeans.lib.awtextra.AbsoluteConstraints(144, 172, 120, 40));

        dia20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia20ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia20, new org.netbeans.lib.awtextra.AbsoluteConstraints(273, 172, 120, 40));

        dia21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia21ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia21, new org.netbeans.lib.awtextra.AbsoluteConstraints(408, 172, 120, 40));

        dia22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia22ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia22, new org.netbeans.lib.awtextra.AbsoluteConstraints(537, 172, 120, 40));

        dia23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia23ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia23, new org.netbeans.lib.awtextra.AbsoluteConstraints(666, 172, 120, 40));

        dia24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia24ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia24, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 221, 120, 40));

        dia25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia25ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia25, new org.netbeans.lib.awtextra.AbsoluteConstraints(144, 221, 120, 40));

        dia26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia26ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia26, new org.netbeans.lib.awtextra.AbsoluteConstraints(273, 221, 120, 40));

        dia27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia27ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia27, new org.netbeans.lib.awtextra.AbsoluteConstraints(408, 221, 120, 40));

        dia28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia28ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia28, new org.netbeans.lib.awtextra.AbsoluteConstraints(537, 221, 120, 40));

        dia29.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia29ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia29, new org.netbeans.lib.awtextra.AbsoluteConstraints(666, 221, 120, 40));

        dia30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia30ActionPerformed(evt);
            }
        });
        vistaMensual.add(dia30, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 270, 120, 40));

        jButton40.setText("Regresar");
        jButton40.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton40ActionPerformed(evt);
            }
        });
        vistaMensual.add(jButton40, new org.netbeans.lib.awtextra.AbsoluteConstraints(691, 351, -1, -1));

        jLabel26.setText("Año:");
        vistaMensual.add(jLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(84, 345, -1, -1));

        tf_anioCalendario.setEditable(false);
        tf_anioCalendario.setText("2016");
        vistaMensual.add(tf_anioCalendario, new org.netbeans.lib.awtextra.AbsoluteConstraints(125, 342, -1, -1));

        jLabel25.setText("Mes:");
        vistaMensual.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(236, 345, -1, -1));

        cb_mesCalendario.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));
        cb_mesCalendario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_mesCalendarioActionPerformed(evt);
            }
        });
        vistaMensual.add(cb_mesCalendario, new org.netbeans.lib.awtextra.AbsoluteConstraints(275, 342, 132, -1));

        jButton8.setText("<");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        vistaMensual.add(jButton8, new org.netbeans.lib.awtextra.AbsoluteConstraints(33, 341, -1, -1));

        jButton9.setText(">");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        vistaMensual.add(jButton9, new org.netbeans.lib.awtextra.AbsoluteConstraints(176, 341, -1, -1));
        vistaMensual.add(filler1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 800, -1));

        menuPanelCambio.addTab("Vista Mensual", vistaMensual);

        dia0Semana.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia0SemanaActionPerformed(evt);
            }
        });

        dia1Semana.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia1SemanaActionPerformed(evt);
            }
        });

        dia2Semana.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia2SemanaActionPerformed(evt);
            }
        });

        dia3Semana.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia3SemanaActionPerformed(evt);
            }
        });

        dia4Semana.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia4SemanaActionPerformed(evt);
            }
        });

        dia5Semana.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia5SemanaActionPerformed(evt);
            }
        });

        dia6Semana.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dia6SemanaActionPerformed(evt);
            }
        });

        cb_semanaSemanal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Semana 1", "Semana 2", "Semana 3", "Semana 4", "Semana 5" }));
        cb_semanaSemanal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_semanaSemanalActionPerformed(evt);
            }
        });

        cb_mesSemanal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));
        cb_mesSemanal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_mesSemanalActionPerformed(evt);
            }
        });

        jLabel27.setText("Año:");

        tf_anioCalendario1.setEditable(false);
        tf_anioCalendario1.setText("2016");

        jButton17.setText("<");
        jButton17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton17ActionPerformed(evt);
            }
        });

        jButton18.setText(">");
        jButton18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton18ActionPerformed(evt);
            }
        });

        jButton10.setText("Regresar");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout vistaSemanalLayout = new javax.swing.GroupLayout(vistaSemanal);
        vistaSemanal.setLayout(vistaSemanalLayout);
        vistaSemanalLayout.setHorizontalGroup(
            vistaSemanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(vistaSemanalLayout.createSequentialGroup()
                .addGroup(vistaSemanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(vistaSemanalLayout.createSequentialGroup()
                        .addGap(73, 73, 73)
                        .addGroup(vistaSemanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dia4Semana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dia0Semana, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(vistaSemanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dia1Semana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(dia5Semana, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(vistaSemanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dia2Semana, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                            .addComponent(dia6Semana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(vistaSemanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dia3Semana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cb_semanaSemanal, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(vistaSemanalLayout.createSequentialGroup()
                        .addGap(134, 134, 134)
                        .addComponent(jButton17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel27)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tf_anioCalendario1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton18)
                        .addGap(18, 18, 18)
                        .addComponent(cb_mesSemanal, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(229, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, vistaSemanalLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jButton10)
                .addGap(45, 45, 45))
        );
        vistaSemanalLayout.setVerticalGroup(
            vistaSemanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(vistaSemanalLayout.createSequentialGroup()
                .addGap(91, 91, 91)
                .addGroup(vistaSemanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(dia1Semana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dia0Semana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dia2Semana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dia3Semana, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(54, 54, 54)
                .addGroup(vistaSemanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(dia4Semana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dia5Semana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dia6Semana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cb_semanaSemanal, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(56, 56, 56)
                .addGroup(vistaSemanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(vistaSemanalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel27)
                        .addComponent(tf_anioCalendario1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton17)
                        .addComponent(jButton18))
                    .addComponent(cb_mesSemanal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 61, Short.MAX_VALUE)
                .addComponent(jButton10)
                .addGap(29, 29, 29))
        );

        menuPanelCambio.addTab("Vista Semanal", vistaSemanal);

        javax.swing.GroupLayout panelCalendarioLayout = new javax.swing.GroupLayout(panelCalendario);
        panelCalendario.setLayout(panelCalendarioLayout);
        panelCalendarioLayout.setHorizontalGroup(
            panelCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(menuPanelCambio)
        );
        panelCalendarioLayout.setVerticalGroup(
            panelCalendarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(menuPanelCambio)
        );

        Panel.add(panelCalendario, "card10");

        jLabel29.setText("Seleccione el Servicio:");

        cb_servicioConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_servicioConsultaActionPerformed(evt);
            }
        });

        tf_nombreServicioConsulta.setEditable(false);

        ta_descripcionServicioConsulta.setEditable(false);
        ta_descripcionServicioConsulta.setColumns(20);
        ta_descripcionServicioConsulta.setLineWrap(true);
        ta_descripcionServicioConsulta.setRows(5);
        jScrollPane3.setViewportView(ta_descripcionServicioConsulta);

        jButton11.setText("Regresar");
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jLabel41.setText("Nombre:");

        jLabel42.setText("Descripcion:");

        javax.swing.GroupLayout panelConsultarServicioLayout = new javax.swing.GroupLayout(panelConsultarServicio);
        panelConsultarServicio.setLayout(panelConsultarServicioLayout);
        panelConsultarServicioLayout.setHorizontalGroup(
            panelConsultarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelConsultarServicioLayout.createSequentialGroup()
                .addContainerGap(700, Short.MAX_VALUE)
                .addComponent(jButton11)
                .addGap(44, 44, 44))
            .addGroup(panelConsultarServicioLayout.createSequentialGroup()
                .addGap(151, 151, 151)
                .addGroup(panelConsultarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel41)
                    .addComponent(jLabel42)
                    .addComponent(jLabel29))
                .addGap(29, 29, 29)
                .addGroup(panelConsultarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tf_nombreServicioConsulta, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                    .addComponent(cb_servicioConsulta, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelConsultarServicioLayout.setVerticalGroup(
            panelConsultarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConsultarServicioLayout.createSequentialGroup()
                .addContainerGap(142, Short.MAX_VALUE)
                .addGroup(panelConsultarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(cb_servicioConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(panelConsultarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tf_nombreServicioConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel41))
                .addGap(28, 28, 28)
                .addGroup(panelConsultarServicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel42))
                .addGap(72, 72, 72)
                .addComponent(jButton11)
                .addGap(26, 26, 26))
        );

        Panel.add(panelConsultarServicio, "card11");

        jLabel30.setText("Seleccione la cita:");

        cb_citasModificarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_citasModificarCitaActionPerformed(evt);
            }
        });

        jLabel33.setText("Fecha:");

        cb_anioModificarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_anioModificarCitaActionPerformed(evt);
            }
        });

        cb_mesModificarCita.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));
        cb_mesModificarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_mesModificarCitaActionPerformed(evt);
            }
        });

        cb_diaModificarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_diaModificarCitaActionPerformed(evt);
            }
        });

        jLabel34.setText("Servicio:");

        cb_serviciosModificarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_serviciosModificarCitaActionPerformed(evt);
            }
        });

        ta_descripcionModificarCita.setEditable(false);
        ta_descripcionModificarCita.setColumns(20);
        ta_descripcionModificarCita.setLineWrap(true);
        ta_descripcionModificarCita.setRows(5);
        jScrollPane4.setViewportView(ta_descripcionModificarCita);

        jLabel35.setText("Hora:");

        btn_modificarCita.setText("Guardar Cambios");
        btn_modificarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_modificarCitaActionPerformed(evt);
            }
        });

        jButton13.setText("Regresar");
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });

        jLabel44.setText("Seleccione el Cliente:");

        cb_clienteModificarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_clienteModificarCitaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelModificarCitaLayout = new javax.swing.GroupLayout(panelModificarCita);
        panelModificarCita.setLayout(panelModificarCitaLayout);
        panelModificarCitaLayout.setHorizontalGroup(
            panelModificarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModificarCitaLayout.createSequentialGroup()
                .addGap(0, 117, Short.MAX_VALUE)
                .addGroup(panelModificarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelModificarCitaLayout.createSequentialGroup()
                        .addComponent(btn_modificarCita)
                        .addGap(169, 169, 169)
                        .addComponent(jButton13))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelModificarCitaLayout.createSequentialGroup()
                        .addGroup(panelModificarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 294, Short.MAX_VALUE)
                            .addGroup(panelModificarCitaLayout.createSequentialGroup()
                                .addComponent(jLabel34)
                                .addGap(18, 18, 18)
                                .addComponent(cb_serviciosModificarCita, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(62, 62, 62)
                        .addComponent(jLabel35)
                        .addGap(15, 15, 15)
                        .addComponent(cb_horaModificarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(149, 149, 149))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelModificarCitaLayout.createSequentialGroup()
                        .addComponent(jLabel33)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cb_anioModificarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addComponent(cb_mesModificarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cb_diaModificarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(158, 158, 158))
                    .addGroup(panelModificarCitaLayout.createSequentialGroup()
                        .addGap(79, 79, 79)
                        .addGroup(panelModificarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelModificarCitaLayout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addComponent(jLabel30)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cb_citasModificarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(2, 2, 2))
                            .addGroup(panelModificarCitaLayout.createSequentialGroup()
                                .addComponent(jLabel44)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cb_clienteModificarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(62, Short.MAX_VALUE))
        );
        panelModificarCitaLayout.setVerticalGroup(
            panelModificarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModificarCitaLayout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addGroup(panelModificarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel44)
                    .addComponent(cb_clienteModificarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addGroup(panelModificarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cb_citasModificarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel30))
                .addGap(48, 48, 48)
                .addGroup(panelModificarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel33)
                    .addComponent(cb_anioModificarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_mesModificarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_diaModificarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addGroup(panelModificarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel34)
                    .addComponent(cb_serviciosModificarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addGroup(panelModificarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelModificarCitaLayout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(panelModificarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelModificarCitaLayout.createSequentialGroup()
                                .addGap(59, 59, 59)
                                .addComponent(jButton13)
                                .addGap(26, 26, 26))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelModificarCitaLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_modificarCita)
                                .addGap(39, 39, 39))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelModificarCitaLayout.createSequentialGroup()
                        .addGroup(panelModificarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel35)
                            .addComponent(cb_horaModificarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(153, 153, 153))))
        );

        Panel.add(panelModificarCita, "card12");

        jLabel39.setText("Seleccione la cita que desea confirmar");

        cb_confirmarCita.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jButton6.setText("Confirmar");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton15.setText("Cancelar");
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });

        jLabel45.setText("Seleccione el cliente");

        cb_clienteConfirmar.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cb_clienteConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_clienteConfirmarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelConfirmarCitaLayout = new javax.swing.GroupLayout(panelConfirmarCita);
        panelConfirmarCita.setLayout(panelConfirmarCitaLayout);
        panelConfirmarCitaLayout.setHorizontalGroup(
            panelConfirmarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelConfirmarCitaLayout.createSequentialGroup()
                .addContainerGap(270, Short.MAX_VALUE)
                .addGroup(panelConfirmarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelConfirmarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(panelConfirmarCitaLayout.createSequentialGroup()
                            .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jButton15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(jLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cb_confirmarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(cb_clienteConfirmar, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(256, 256, 256))
        );
        panelConfirmarCitaLayout.setVerticalGroup(
            panelConfirmarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConfirmarCitaLayout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addComponent(jLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_clienteConfirmar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jLabel39, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_confirmarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(75, 75, 75)
                .addGroup(panelConfirmarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(87, Short.MAX_VALUE))
        );

        Panel.add(panelConfirmarCita, "card13");

        jLabel36.setText("Seleccione un cliente:");

        tbn_agrgearListaEspera.setText("Agregar a lista");
        tbn_agrgearListaEspera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tbn_agrgearListaEsperaActionPerformed(evt);
            }
        });

        btn_cancelarListaEspera.setText("Cancelar");
        btn_cancelarListaEspera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarListaEsperaActionPerformed(evt);
            }
        });

        btn_eliminarListaEspera.setText("Eliminar de lista");
        btn_eliminarListaEspera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_eliminarListaEsperaActionPerformed(evt);
            }
        });

        jLabel50.setText("Lista de Espera:");

        javax.swing.GroupLayout panelListaEsperaLayout = new javax.swing.GroupLayout(panelListaEspera);
        panelListaEspera.setLayout(panelListaEsperaLayout);
        panelListaEsperaLayout.setHorizontalGroup(
            panelListaEsperaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelListaEsperaLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_cancelarListaEspera, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27))
            .addGroup(panelListaEsperaLayout.createSequentialGroup()
                .addGap(127, 127, 127)
                .addGroup(panelListaEsperaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelListaEsperaLayout.createSequentialGroup()
                        .addGap(251, 251, 251)
                        .addComponent(tbn_agrgearListaEspera, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelListaEsperaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(panelListaEsperaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelListaEsperaLayout.createSequentialGroup()
                                .addComponent(jLabel50)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(listaEspera, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelListaEsperaLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_eliminarListaEspera, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(78, 78, 78)))
                        .addGroup(panelListaEsperaLayout.createSequentialGroup()
                            .addComponent(jLabel36)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(cb_listaEspera, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(176, Short.MAX_VALUE))
        );
        panelListaEsperaLayout.setVerticalGroup(
            panelListaEsperaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelListaEsperaLayout.createSequentialGroup()
                .addGap(143, 143, 143)
                .addGroup(panelListaEsperaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_listaEspera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(tbn_agrgearListaEspera)
                .addGap(54, 54, 54)
                .addGroup(panelListaEsperaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel50)
                    .addComponent(listaEspera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btn_eliminarListaEspera, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(102, 102, 102)
                .addComponent(btn_cancelarListaEspera)
                .addGap(31, 31, 31))
        );

        Panel.add(panelListaEspera, "card14");

        jLabel37.setText("Clientes en lista de Espera");

        ta_clienteListaEspera.setColumns(20);
        ta_clienteListaEspera.setRows(5);
        jScrollPane5.setViewportView(ta_clienteListaEspera);

        jButton2.setText("Regresar al Menu Principal");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelConsultarListaEsperaLayout = new javax.swing.GroupLayout(panelConsultarListaEspera);
        panelConsultarListaEspera.setLayout(panelConsultarListaEsperaLayout);
        panelConsultarListaEsperaLayout.setHorizontalGroup(
            panelConsultarListaEsperaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConsultarListaEsperaLayout.createSequentialGroup()
                .addContainerGap(140, Short.MAX_VALUE)
                .addGroup(panelConsultarListaEsperaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelConsultarListaEsperaLayout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelConsultarListaEsperaLayout.createSequentialGroup()
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(72, 72, 72)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addGap(127, 127, 127))
        );
        panelConsultarListaEsperaLayout.setVerticalGroup(
            panelConsultarListaEsperaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConsultarListaEsperaLayout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(jLabel37, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(panelConsultarListaEsperaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelConsultarListaEsperaLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(49, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelConsultarListaEsperaLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2)
                        .addGap(228, 228, 228))))
        );

        Panel.add(panelConsultarListaEspera, "card15");

        jLabel40.setText("Seleccione el cliente");

        cb_eliminarCitaCliente.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cb_eliminarCitaCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_eliminarCitaClienteActionPerformed(evt);
            }
        });

        btn_eliminarCita.setText("Eliminar");
        btn_eliminarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_eliminarCitaActionPerformed(evt);
            }
        });

        jButton19.setText("Cancelar");
        jButton19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton19ActionPerformed(evt);
            }
        });

        jLabel43.setText("Seleccione la cita");

        cb_citaEliminarCita.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout panelEliminarCitaLayout = new javax.swing.GroupLayout(panelEliminarCita);
        panelEliminarCita.setLayout(panelEliminarCitaLayout);
        panelEliminarCitaLayout.setHorizontalGroup(
            panelEliminarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelEliminarCitaLayout.createSequentialGroup()
                .addContainerGap(258, Short.MAX_VALUE)
                .addGroup(panelEliminarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_eliminarCitaCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelEliminarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(cb_citaEliminarCita, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panelEliminarCitaLayout.createSequentialGroup()
                            .addComponent(btn_eliminarCita, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(49, 49, 49)
                            .addComponent(jButton19, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(247, 247, 247))
        );
        panelEliminarCitaLayout.setVerticalGroup(
            panelEliminarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelEliminarCitaLayout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cb_eliminarCitaCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel43, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cb_citaEliminarCita, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(66, 66, 66)
                .addGroup(panelEliminarCitaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_eliminarCita)
                    .addComponent(jButton19))
                .addContainerGap(141, Short.MAX_VALUE))
        );

        Panel.add(panelEliminarCita, "card16");

        jLabel46.setText("Seleccione el cliente:");

        cb_correo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cb_correo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_correoActionPerformed(evt);
            }
        });

        jLabel47.setText("Asunto");

        tf_CorreoAsunto.setText("jTextField1");

        tf_Correocorreo.setText("jTextField1");

        jLabel48.setText("Email:");

        jLabel49.setText("Texto:");

        ta_CorreoDescripcion.setColumns(20);
        ta_CorreoDescripcion.setRows(5);
        jScrollPane6.setViewportView(ta_CorreoDescripcion);

        btn_mandarCorreo.setText("Enviar");
        btn_mandarCorreo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_mandarCorreoActionPerformed(evt);
            }
        });

        jButton3.setText("Cancelar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelCorreoLayout = new javax.swing.GroupLayout(panelCorreo);
        panelCorreo.setLayout(panelCorreoLayout);
        panelCorreoLayout.setHorizontalGroup(
            panelCorreoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCorreoLayout.createSequentialGroup()
                .addGap(168, 168, 168)
                .addGroup(panelCorreoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCorreoLayout.createSequentialGroup()
                        .addGroup(panelCorreoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel46)
                            .addComponent(jLabel47)
                            .addComponent(jLabel48)
                            .addComponent(jLabel49))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelCorreoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cb_correo, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panelCorreoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(tf_Correocorreo, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(tf_CorreoAsunto, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCorreoLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_mandarCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(72, 72, 72)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 101, Short.MAX_VALUE)
                .addComponent(jButton3)
                .addGap(31, 31, 31))
        );
        panelCorreoLayout.setVerticalGroup(
            panelCorreoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCorreoLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(panelCorreoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_correo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(panelCorreoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tf_CorreoAsunto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel47))
                .addGap(39, 39, 39)
                .addGroup(panelCorreoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tf_Correocorreo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel48))
                .addGroup(panelCorreoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelCorreoLayout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelCorreoLayout.createSequentialGroup()
                        .addGap(123, 123, 123)
                        .addComponent(jLabel49)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                .addGroup(panelCorreoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCorreoLayout.createSequentialGroup()
                        .addComponent(btn_mandarCorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(36, 36, 36))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCorreoLayout.createSequentialGroup()
                        .addComponent(jButton3)
                        .addGap(25, 25, 25))))
        );

        Panel.add(panelCorreo, "card17");

        menuClientes.setText("Clientes");
        menuClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClientesActionPerformed(evt);
            }
        });

        registrarCliente.setText("Registrar Nuevo Cliente");
        registrarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registrarClienteActionPerformed(evt);
            }
        });
        menuClientes.add(registrarCliente);

        modificarCliente.setText("Modificar Datos de un Cliente");
        modificarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarClienteActionPerformed(evt);
            }
        });
        menuClientes.add(modificarCliente);

        eliminarCliente.setText("Eliminar Cliente del Sistema");
        eliminarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarClienteActionPerformed(evt);
            }
        });
        menuClientes.add(eliminarCliente);
        menuClientes.add(separadorCliente);

        consultarCliente.setText("Consultar Cliente");
        consultarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarClienteActionPerformed(evt);
            }
        });
        menuClientes.add(consultarCliente);

        barraOperaciones.add(menuClientes);

        menuCitas.setText("Citas");

        crearCita.setText("Crear Cita");
        crearCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crearCitaActionPerformed(evt);
            }
        });
        menuCitas.add(crearCita);

        modificarCita.setText("Modificar Cita");
        modificarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarCitaActionPerformed(evt);
            }
        });
        menuCitas.add(modificarCita);

        jMenuItem3.setText("Eliminar Cita");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        menuCitas.add(jMenuItem3);
        menuCitas.add(jSeparator2);

        confirmarCita.setText("Confirmar Cita");
        confirmarCita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmarCitaActionPerformed(evt);
            }
        });
        menuCitas.add(confirmarCita);
        menuCitas.add(jSeparator3);

        calendario.setText("Calendario");
        calendario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calendarioActionPerformed(evt);
            }
        });
        menuCitas.add(calendario);

        barraOperaciones.add(menuCitas);

        menuServicios.setText("Servicios");
        menuServicios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuServiciosActionPerformed(evt);
            }
        });

        manejarServicios.setText("Manejar Servicios");
        manejarServicios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manejarServiciosActionPerformed(evt);
            }
        });
        menuServicios.add(manejarServicios);
        menuServicios.add(jSeparator1);

        consultarServicio.setText("Consultar Servicio");
        consultarServicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consultarServicioActionPerformed(evt);
            }
        });
        menuServicios.add(consultarServicio);

        barraOperaciones.add(menuServicios);

        menuAdministracion.setText("Administracion");
        menuAdministracion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAdministracionActionPerformed(evt);
            }
        });

        horarioSistema.setText("Horario");
        horarioSistema.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                horarioSistemaActionPerformed(evt);
            }
        });
        menuAdministracion.add(horarioSistema);
        menuAdministracion.add(jSeparator4);

        jMenuItem4.setText("Enviar Correo");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        menuAdministracion.add(jMenuItem4);
        menuAdministracion.add(jSeparator5);

        jMenuItem1.setText("Lista de Espera");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        menuAdministracion.add(jMenuItem1);

        jMenuItem2.setText("Consultar Lista Espera");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        menuAdministracion.add(jMenuItem2);

        barraOperaciones.add(menuAdministracion);

        setJMenuBar(barraOperaciones);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(Panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void eliminarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarClienteActionPerformed
        if (sistema.getListaClientes().size() > 0) {
            cb_eliminarCliente.removeAllItems();
            cb_eliminarCliente.addItem("");
            for (int i = 0; i < sistema.getListaClientes().size(); i++) {
                cb_eliminarCliente.addItem(sistema.getListaClientes().get(i).getNombre() + " " +
                        sistema.getListaClientes().get(i).getApellido1() + " - " + 
                        sistema.getListaClientes().get(i).getTelefono());
            }
            tf_eliminarNombreCliente.setText("");
            tf_eliminarApellido1Cliente.setText("");
            tf_eliminarApellido2Cliente.setText("");
            tf_eliminarCorreoCliente.setText("");
            tf_eliminarTelefonoCliente.setText("");
            Panel.removeAll();
            Panel.add(panelEliminarCliente);
            Panel.repaint();
            Panel.revalidate();
        }
        else {
            JOptionPane.showMessageDialog(null, "No existe ningun cliente en el sistema.");
        }
    }//GEN-LAST:event_eliminarClienteActionPerformed

    private void btn_cancelarRegistrarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarRegistrarClienteActionPerformed
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_btn_cancelarRegistrarClienteActionPerformed

    private void registrarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registrarClienteActionPerformed
        tf_registrarNombreCliente.setText("");
        tf_registrarApellido1Cliente.setText("");
        tf_registrarApellido2Cliente.setText("");
        tf_registrarCorreoCliente.setText("");
        tf_registrarTelefonoCliente.setText("");
        Panel.removeAll();
        Panel.add(panelRegistrarCliente);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_registrarClienteActionPerformed

    private void btn_registrarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_registrarClienteActionPerformed
        String nombre = tf_registrarNombreCliente.getText();
        String apellido1 = tf_registrarApellido1Cliente.getText();
        String apellido2 = tf_registrarApellido2Cliente.getText();
        String correo = tf_registrarCorreoCliente.getText();
        String telefono = tf_registrarTelefonoCliente.getText();
        if (sistema.registrarCliente(nombre, apellido1, apellido2, correo, telefono)){
            tf_registrarNombreCliente.setText("");
            tf_registrarApellido1Cliente.setText("");
            tf_registrarApellido2Cliente.setText("");
            tf_registrarCorreoCliente.setText("");
            tf_registrarTelefonoCliente.setText("");
        }
    }//GEN-LAST:event_btn_registrarClienteActionPerformed

    private void modificarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarClienteActionPerformed
        if (sistema.getListaClientes().size() == 0) {
            JOptionPane.showMessageDialog(null, "No hay clientes registrados");
        }else{
            cb_modificarCliente.removeAllItems();
            cb_modificarCliente.addItem("");
            for (int i = 0; i < sistema.getListaClientes().size(); i++) {
                String entrada = sistema.getListaClientes().get(i).getNombre() + " " +
                        sistema.getListaClientes().get(i).getApellido1() + " " + 
                        sistema.getListaClientes().get(i).getApellido2() + " Tel: " +
                        sistema.getListaClientes().get(i).getTelefono() + "";
                cb_modificarCliente.addItem(entrada);
            }
            tf_modificarNombreCliente.setText("");
            tf_modificarApellido1Cliente.setText("");
            tf_modificarApellido2Cliente.setText("");
            tf_modificarCorreoCliente.setText("");
            tf_modificarTelefonoCliente.setText("");
            Panel.removeAll();
            Panel.add(panelModificarCliente);
            Panel.repaint();
            Panel.revalidate();
        }
    }//GEN-LAST:event_modificarClienteActionPerformed

    private void btn_cancelaModificarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelaModificarClienteActionPerformed
        // TODO add your handling code here:
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_btn_cancelaModificarClienteActionPerformed

    private void cb_modificarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_modificarClienteActionPerformed
        int indice = cb_modificarCliente.getSelectedIndex();
        if (indice <= 0) {
            tf_modificarNombreCliente.setText("");
            tf_modificarApellido1Cliente.setText("");
            tf_modificarApellido2Cliente.setText("");
            tf_modificarCorreoCliente.setText("");
            tf_modificarTelefonoCliente.setText("");
        }
        else {
            indice -= 1;
            tf_modificarNombreCliente.setText(sistema.getListaClientes().get(indice).getNombre());
            tf_modificarApellido1Cliente.setText(sistema.getListaClientes().get(indice).getApellido1());
            tf_modificarApellido2Cliente.setText(sistema.getListaClientes().get(indice).getApellido2());
            tf_modificarCorreoCliente.setText(sistema.getListaClientes().get(indice).getCorreo());
            tf_modificarTelefonoCliente.setText(sistema.getListaClientes().get(indice).getTelefono() + "");
        }
    }//GEN-LAST:event_cb_modificarClienteActionPerformed

    private void btn_cancelarEliminarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarEliminarClienteActionPerformed
        // TODO add your handling code here:
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_btn_cancelarEliminarClienteActionPerformed

    private void cb_eliminarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_eliminarClienteActionPerformed
        int indice = cb_eliminarCliente.getSelectedIndex();
        if (indice == 0) {
            tf_eliminarNombreCliente.setText("");
            tf_eliminarApellido1Cliente.setText("");
            tf_eliminarApellido2Cliente.setText("");
            tf_eliminarCorreoCliente.setText("");
            tf_eliminarTelefonoCliente.setText("");
        }
        else {
            indice -= 1;
            tf_eliminarNombreCliente.setText(sistema.getListaClientes().get(indice).getNombre());
            tf_eliminarApellido1Cliente.setText(sistema.getListaClientes().get(indice).getApellido1());
            tf_eliminarApellido2Cliente.setText(sistema.getListaClientes().get(indice).getApellido2());
            tf_eliminarCorreoCliente.setText(sistema.getListaClientes().get(indice).getCorreo());
            tf_eliminarTelefonoCliente.setText(sistema.getListaClientes().get(indice).getTelefono() + "");
        }
    }//GEN-LAST:event_cb_eliminarClienteActionPerformed

    private void btn_modificarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_modificarClienteActionPerformed
        if (cb_modificarCliente.getSelectedIndex() > 0) {
            String nombre = tf_modificarNombreCliente.getText();
            String apellido1 = tf_modificarApellido1Cliente.getText();
            String apellido2 = tf_modificarApellido2Cliente.getText();
            String correo = tf_modificarCorreoCliente.getText();
            String telefono = tf_modificarTelefonoCliente.getText();
            int indice = cb_modificarCliente.getSelectedIndex() - 1;
            if (sistema.modificarCliente(indice, nombre, apellido1, apellido2, correo, telefono)){
                cb_modificarCliente.removeAllItems();
                cb_modificarCliente.addItem("");
                for (int i = 0; i < sistema.getListaClientes().size(); i++) {
                    cb_modificarCliente.addItem(sistema.getListaClientes().get(i).getNombre() + " " +
                    sistema.getListaClientes().get(i).getApellido1() + " - " + 
                    sistema.getListaClientes().get(i).getTelefono());
                }
                tf_modificarNombreCliente.setText("");
                tf_modificarApellido1Cliente.setText("");
                tf_modificarApellido2Cliente.setText("");
                tf_modificarCorreoCliente.setText("");
                tf_modificarTelefonoCliente.setText("");
            }    
        }else {
            JOptionPane.showMessageDialog(null, "Favor de escojer un cliente.");
        }
    }//GEN-LAST:event_btn_modificarClienteActionPerformed

    private void btn_eliminarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_eliminarClienteActionPerformed
        int indiceCliente = cb_eliminarCliente.getSelectedIndex();
        if (indiceCliente > 0) {
            indiceCliente -= 1;
            UIManager.put("OptionPane.yesButtonText", "Si");
            int reply = JOptionPane.showConfirmDialog(null, "Esta seguro que desea eliminar el cliente?"
                                                      , "Eliminar Cliente", JOptionPane.YES_NO_OPTION);
            if (reply == JOptionPane.YES_OPTION) {
                if (sistema.eliminarCliente(sistema.getListaClientes().get(indiceCliente))){
                  cb_eliminarCliente.removeAllItems();
                    cb_eliminarCliente.addItem("");
                    for (int i = 0; i < sistema.getListaClientes().size(); i++) {
                        cb_eliminarCliente.addItem(sistema.getListaClientes().get(i).getNombre() + " " +
                            sistema.getListaClientes().get(i).getApellido1() + " - " + 
                            sistema.getListaClientes().get(i).getTelefono());
                    }
                    Panel.removeAll();
                    Panel.add(panelModificarCliente);
                    Panel.repaint();
                    Panel.revalidate();
                }  
            }   
        }else {
            JOptionPane.showMessageDialog(null, "Favor de escojer un cliente.");
        }
    }//GEN-LAST:event_btn_eliminarClienteActionPerformed

    private void cb_consultarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_consultarClienteActionPerformed
        int indice = cb_consultarCliente.getSelectedIndex();
        if (indice <= 0) {
            tf_consultarClienteNombre.setText("");
            tf_consultarClienteApellido1.setText("");
            tf_consultarClienteApellido2.setText("");
            tf_consultarClienteCorreo.setText("");
            tf_consultarClienteTelefono.setText("");
        }
        else {
            indice -= 1;
            tf_consultarClienteNombre.setText(sistema.getListaClientes().get(indice).getNombre());
            tf_consultarClienteApellido1.setText(sistema.getListaClientes().get(indice).getApellido1());
            tf_consultarClienteApellido2.setText(sistema.getListaClientes().get(indice).getApellido2());
            tf_consultarClienteCorreo.setText(sistema.getListaClientes().get(indice).getCorreo());
            tf_consultarClienteTelefono.setText(sistema.getListaClientes().get(indice).getTelefono() + "");
        }
    }//GEN-LAST:event_cb_consultarClienteActionPerformed

    private void btn_cancelarConsultarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarConsultarClienteActionPerformed
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_btn_cancelarConsultarClienteActionPerformed

    private void menuClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuClientesActionPerformed

    }//GEN-LAST:event_menuClientesActionPerformed

    private void consultarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarClienteActionPerformed
        if (sistema.getListaClientes().size() > 0) {
            cb_consultarCliente.removeAllItems();
            cb_consultarCliente.addItem("");
            for (int i = 0; i < sistema.getListaClientes().size(); i++) {
                cb_consultarCliente.addItem(sistema.getListaClientes().get(i).getNombre() + " " +
                            sistema.getListaClientes().get(i).getApellido1() + " - " + 
                            sistema.getListaClientes().get(i).getTelefono());
            }
            Panel.removeAll();
            Panel.add(panelConsultarCliente);
            Panel.repaint();
            Panel.revalidate();
        }
        else {
            JOptionPane.showMessageDialog(null, "No existe ningun cliente en el sistema.");
        }
    }//GEN-LAST:event_consultarClienteActionPerformed

    private void manejarServiciosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manejarServiciosActionPerformed
        cb_manejarServicios.removeAllItems();
        cb_manejarServicios.addItem("");
        for (int i = 0; i <  sistema.getListaServicios().size(); i++) {
            cb_manejarServicios.addItem(sistema.getListaServicios().get(i).getNombre());
        }
        tf_nombreServicio.setText("");
        ta_descripcionServicio.setText("");
        Panel.removeAll();
        Panel.add(panelManejoServicios);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_manejarServiciosActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void menuServiciosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuServiciosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_menuServiciosActionPerformed

    private void btn_agregarServicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_agregarServicioActionPerformed
        String nombre = tf_nombreServicio.getText();
        String descripcion = ta_descripcionServicio.getText();
        if (sistema.registrarServicio(nombre, descripcion) ){
           cb_manejarServicios.removeAllItems();
            cb_manejarServicios.addItem("");
            for (int i = 0; i < sistema.getListaServicios().size(); i++) {
                cb_manejarServicios.addItem(sistema.getListaServicios().get(i).getNombre());
            } 
        }
    }//GEN-LAST:event_btn_agregarServicioActionPerformed

    private void btn_eliminarServicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_eliminarServicioActionPerformed
        if (sistema.getListaServicios().size() == 0){
            JOptionPane.showMessageDialog(null, "No hay serivios registrados");
        }else{
            int indice = cb_manejarServicios.getSelectedIndex();
         UIManager.put("OptionPane.yesButtonText", "Si");
        int reply = JOptionPane.showConfirmDialog(null, "Esta seguro que desea eliminar el servicio?"
                                                      , "Eliminar Cliente", JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION){
            sistema.removerServicio(indice);

        }
        cb_manejarServicios.removeAllItems();
        cb_manejarServicios.addItem("");
        for (int i = 0; i < sistema.getListaServicios().size(); i++) {
            cb_manejarServicios.addItem(sistema.getListaServicios().get(i).getNombre());
        }
        tf_nombreServicio.setText("");
        ta_descripcionServicio.setText("");
        }
    }//GEN-LAST:event_btn_eliminarServicioActionPerformed

    private void cb_manejarServiciosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_manejarServiciosActionPerformed
        int indice = cb_manejarServicios.getSelectedIndex();
        if (indice < 1) {
            tf_nombreServicio.setText("");
            ta_descripcionServicio.setText("");
        }
        else {
            indice -= 1;
            tf_nombreServicio.setText(sistema.getListaServicios().get(indice).getNombre());
            ta_descripcionServicio.setText(sistema.getListaServicios().get(indice).getDescripcion());
        }
    }//GEN-LAST:event_cb_manejarServiciosActionPerformed

    private void btn_modificarServicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_modificarServicioActionPerformed
        int indice = cb_manejarServicios.getSelectedIndex();
        String nombre = tf_nombreServicio.getText();
        String descripcion = ta_descripcionServicio.getText();
        if (sistema.modificarServicio(indice, nombre, descripcion) ){
            cb_manejarServicios.removeAllItems();
            cb_manejarServicios.addItem("");
            for (int i = 0; i < sistema.getListaServicios().size(); i++) {
                cb_manejarServicios.addItem(sistema.getListaServicios().get(i).getNombre());
            }
            tf_nombreServicio.setText("");
            ta_descripcionServicio.setText("");
        }else{
            cb_manejarServicios.setSelectedIndex(0);
            tf_nombreServicio.setText("");
            ta_descripcionServicio.setText("");
        }        
    }//GEN-LAST:event_btn_modificarServicioActionPerformed

    private void crearCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crearCitaActionPerformed
        if (sistema.getListaClientes().size() == 0) {
            JOptionPane.showMessageDialog(null, "No existe ningun cliente registrado.");
        }else if (sistema.getListaServicios().size() == 0) {
            JOptionPane.showMessageDialog(null, "No existe ningun servicio en el sistema.");
        }else {
                cb_clienteRegistrarCita.removeAllItems();
                cb_clienteRegistrarCita.addItem("");
                for (int i = 0; i < sistema.getListaClientes().size(); i++) {
                    cb_clienteRegistrarCita.addItem(sistema.getListaClientes().get(i).getNombre() + " " +
                            sistema.getListaClientes().get(i).getApellido1() + " - " +
                            sistema.getListaClientes().get(i).getTelefono());
                }
                cb_servicioRegistrarCita.removeAllItems();
                for (int i = 0; i < sistema.getListaServicios().size(); i++) {
                    cb_servicioRegistrarCita.addItem(sistema.getListaServicios().get(i).getNombre());
                }
                cb_anioRegistrarCita.removeAllItems();
                int anio = Calendar.getInstance().get(Calendar.YEAR);
                int limite = anio + 10;
                while (anio <= limite) {
                    cb_anioRegistrarCita.addItem(anio + "");
                    anio += 1;
                }
                cb_mesRegistrarCita.setSelectedIndex(0);
                Panel.removeAll();
                Panel.add(panelCrearCita);
                Panel.repaint();
                Panel.revalidate();
        }
    }//GEN-LAST:event_crearCitaActionPerformed

    private void cb_horarioDiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_horarioDiaActionPerformed
        int indice = cb_horarioDia.getSelectedIndex();
        if (indice > 0){
            indice = indice - 1;
            String horaInicio = sistema.getListaHorarios().get(indice).getInicioHorario();
            String horaFin = sistema.getListaHorarios().get(indice).getFinHorario();
            cb_HorarioInicio.setSelectedItem(horaInicio);
            cb_HorarioFin.setSelectedItem(horaFin);
        }
        
    }//GEN-LAST:event_cb_horarioDiaActionPerformed

    private void cb_HorarioFinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_HorarioFinActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb_HorarioFinActionPerformed

    private void btn_guardarHorarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarHorarioActionPerformed
        if (cb_HorarioInicio.getSelectedIndex() <= cb_HorarioFin.getSelectedIndex()) {
            int indiceDia = cb_horarioDia.getSelectedIndex();
            if (indiceDia > 0) {
                indiceDia = indiceDia - 1;
                int indiceInicio = cb_HorarioInicio.getSelectedIndex();
                int indiceFin = cb_HorarioFin.getSelectedIndex();
                if (indiceInicio > 0 && indiceFin > 0){
                    indiceInicio = indiceInicio - 1;
                    indiceFin = indiceFin - 1;
                    if (indiceInicio >= indiceFin){
                    JOptionPane.showMessageDialog(null, "No se puede establecer ese horario");
                    }else{
                        String horaInicio = cb_HorarioInicio.getSelectedItem().toString();
                        String horaFin = cb_HorarioFin.getSelectedItem().toString();
                        boolean chocaHorario = false;
                        for (int i = 0; i < sistema.getListaClientes().size(); i++){
                            for (int j = 0; j < sistema.getListaClientes().get(i).getListadoCitas().size(); j++){
                                Calendar calendario = Calendar.getInstance();
                                calendario.set(Calendar.YEAR, sistema.getListaClientes().get(i).getListadoCitas().get(j).getAnio());
                                calendario.set(Calendar.MONTH, sistema.getListaClientes().get(i).getListadoCitas().get(j).getMes());
                                calendario.set(Calendar.DAY_OF_MONTH, sistema.getListaClientes().get(i).getListadoCitas().get(j).getDia());
                                int dia = calendario.get(Calendar.DAY_OF_WEEK); 
                                switch (dia){
                                    case 2:
                                        dia = 0;//lunes
                                        break;
                                    case 3:
                                        dia = 1;
                                        break;
                                    case 4:
                                        dia = 2;
                                        break;
                                    case 5:
                                        dia = 3;
                                        break;
                                    case 6:
                                        dia = 4;
                                        break;
                                    case 7:
                                        dia = 5;
                                        break;
                                    case 1:
                                        dia = 6;
                                        break;
                                }
                                if (dia == indiceDia){
                                    int horaIni = sistema.conseguirHora(horaInicio);
                                    int horaFi = sistema.conseguirHora(horaFin);
                                    if ( horaIni > sistema.conseguirHora(sistema.getListaClientes().get(i).getListadoCitas().get(j).getHora())){
                                         JOptionPane.showMessageDialog(null, "Hay una cita que tenia horario ese dia y ya no perteneceria al horario nuevo");
                                         chocaHorario = true;
                                         break;
                                       
                                    }else if (horaFi < sistema.conseguirHora(sistema.getListaClientes().get(i).getListadoCitas().get(j).getHora())){
                                    JOptionPane.showMessageDialog(null, "Hay una cita que tenia horario ese dia y ya no perteneceria al horario nuevo");
                                    chocaHorario = true;
                                    break;
                                    }
                                }
                            }
                        }   
                        if (chocaHorario == false){
                            sistema.modificarHorario(indiceDia, horaInicio, horaFin);
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Error en horarios seleccionados");
                }
            }else{
                JOptionPane.showMessageDialog(null, "Debe seleccionar un dia");
            }
        }      
    }//GEN-LAST:event_btn_guardarHorarioActionPerformed

    private void horarioSistemaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_horarioSistemaActionPerformed
        cb_horarioDia.setSelectedIndex(0);
        Panel.removeAll();
        cb_horarioDia.removeAllItems();
        cb_horarioDia.addItem("");
        cb_horarioDia.addItem("Lunes");
        cb_horarioDia.addItem("Martes");
        cb_horarioDia.addItem("Miercoles");
        cb_horarioDia.addItem("Jueves");
        cb_horarioDia.addItem("Viernes");
        cb_horarioDia.addItem("Sabado");
        cb_horarioDia.addItem("Domingo");
        
        cb_HorarioInicio.removeAllItems();
        cb_HorarioInicio.addItem("");
        for (int i = 0; i < 12; i++){
            if (i == 0){
                cb_HorarioInicio.addItem("12:00 am");
            }else{
                cb_HorarioInicio.addItem(i + ":00 am");
            }
        }
        for (int i = 0; i < 12; i++){
            if (i == 0){
                cb_HorarioInicio.addItem("12:00 pm");
            }else{
                cb_HorarioInicio.addItem(i + ":00 pm");
            }
        }
        cb_HorarioFin.removeAllItems();
        cb_HorarioFin.addItem("");
        for (int i = 0; i < 12; i++){
            if (i == 0){
                cb_HorarioFin.addItem("12:00 am");
            }else{
                cb_HorarioFin.addItem(i + ":00 am");
            }
        }
        for (int i = 0; i < 12; i++){
            if (i == 0){
                cb_HorarioFin.addItem("12:00 pm");
            }else{
                cb_HorarioFin.addItem(i + ":00 pm");
            }
        }





        Panel.add(panelHorario);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_horarioSistemaActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void dia2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia2ActionPerformed
        int dia = 3;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia2ActionPerformed

    private void dia11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia11ActionPerformed
        int dia = 12;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia11ActionPerformed

    private void jButton40ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton40ActionPerformed
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_jButton40ActionPerformed

    private void cb_mesCalendarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_mesCalendarioActionPerformed
        actualizarCalendarioMes();
        actualizarCalendarioSemana();
    }//GEN-LAST:event_cb_mesCalendarioActionPerformed

    private void dia30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia30ActionPerformed
        int dia = 31;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia30ActionPerformed

    private void dia18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia18ActionPerformed
        int dia = 19;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia18ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio =  new StringInt(anio);
        int year = nuevoAnio.pasar_a_Int();
        year += 1;
        tf_anioCalendario.setText(year + "");
        tf_anioCalendario1.setText(year + "");
        actualizarCalendarioMes();
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio =  new StringInt(anio);
        int year = nuevoAnio.pasar_a_Int();
        year -= 1;
        tf_anioCalendario.setText(year + "");
        tf_anioCalendario1.setText(year + "");
        actualizarCalendarioMes();
    }//GEN-LAST:event_jButton8ActionPerformed

    private void dia1SemanaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia1SemanaActionPerformed
        int indice = cb_semanaSemanal.getSelectedIndex();
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
                mes = 1;
            }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        if (indice <= 0) {
            int dia = 2;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 1) {
            int dia = 9;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 2) {
            int dia = 16;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 3) {
            int dia = 23;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 4) {
            int dia = 30;
            VentanaCita(dia, mes, anioSeleccionado);
        }
    }//GEN-LAST:event_dia1SemanaActionPerformed

    private void cb_mesSemanalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_mesSemanalActionPerformed
        cb_mesCalendario.setSelectedIndex(cb_mesSemanal.getSelectedIndex());
    }//GEN-LAST:event_cb_mesSemanalActionPerformed

    private void jButton17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton17ActionPerformed
        String anio = tf_anioCalendario1.getText();
        StringInt nuevoAnio =  new StringInt(anio);
        int year = nuevoAnio.pasar_a_Int();
        year -= 1;
        tf_anioCalendario.setText(year + "");
        tf_anioCalendario1.setText(year + "");
        actualizarCalendarioMes();
        actualizarCalendarioSemana();
    }//GEN-LAST:event_jButton17ActionPerformed

    private void jButton18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton18ActionPerformed
        String anio = tf_anioCalendario1.getText();
        StringInt nuevoAnio =  new StringInt(anio);
        int year = nuevoAnio.pasar_a_Int();
        year += 1;
        tf_anioCalendario.setText(year + "");
        tf_anioCalendario1.setText(year + "");
        actualizarCalendarioMes();
        actualizarCalendarioSemana();
    }//GEN-LAST:event_jButton18ActionPerformed

    private void cb_semanaSemanalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_semanaSemanalActionPerformed
        actualizarCalendarioSemana();
    }//GEN-LAST:event_cb_semanaSemanalActionPerformed

    private void menuAdministracionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAdministracionActionPerformed

    }//GEN-LAST:event_menuAdministracionActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_jButton10ActionPerformed

    private void dia0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia0ActionPerformed
        int dia = 1;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia0ActionPerformed

    private void dia1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia1ActionPerformed
        int dia = 2;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia1ActionPerformed

    private void dia3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia3ActionPerformed
        int dia = 4;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia3ActionPerformed

    private void dia4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia4ActionPerformed
        int dia = 5;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia4ActionPerformed

    private void dia5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia5ActionPerformed
        int dia = 6;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia5ActionPerformed

    private void dia6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia6ActionPerformed
        int dia = 7;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia6ActionPerformed

    private void dia7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia7ActionPerformed
        int dia = 8;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia7ActionPerformed

    private void dia8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia8ActionPerformed
        int dia = 9;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia8ActionPerformed

    private void dia9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia9ActionPerformed
        int dia = 10;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia9ActionPerformed

    private void dia10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia10ActionPerformed
        int dia = 11;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia10ActionPerformed

    private void dia12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia12ActionPerformed
        int dia = 13;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia12ActionPerformed

    private void dia13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia13ActionPerformed
        int dia = 14;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia13ActionPerformed

    private void dia14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia14ActionPerformed
        int dia = 15;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia14ActionPerformed

    private void dia15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia15ActionPerformed
        int dia = 16;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia15ActionPerformed

    private void dia16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia16ActionPerformed
        int dia = 17;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia16ActionPerformed

    private void dia17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia17ActionPerformed
        int dia = 18;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia17ActionPerformed

    private void dia19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia19ActionPerformed
        int dia = 20;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia19ActionPerformed

    private void dia20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia20ActionPerformed
        int dia = 21;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia20ActionPerformed

    private void dia21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia21ActionPerformed
        int dia = 22;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia21ActionPerformed

    private void dia22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia22ActionPerformed
        int dia = 23;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia22ActionPerformed

    private void dia23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia23ActionPerformed
        int dia = 24;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia23ActionPerformed

    private void dia24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia24ActionPerformed
        int dia = 25;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia24ActionPerformed

    private void dia25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia25ActionPerformed
        int dia = 26;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia25ActionPerformed

    private void dia26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia26ActionPerformed
        int dia = 27;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia26ActionPerformed

    private void dia27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia27ActionPerformed
        int dia = 28;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia27ActionPerformed

    private void dia28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia28ActionPerformed
        int dia = 29;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia28ActionPerformed

    private void dia29ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia29ActionPerformed
        int dia = 30;
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        VentanaCita(dia, mes, anioSeleccionado);
    }//GEN-LAST:event_dia29ActionPerformed

    private void dia0SemanaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia0SemanaActionPerformed
        int indice = cb_semanaSemanal.getSelectedIndex();
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
                mes = 1;
            }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        if (indice <= 0) {
            int dia = 1;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 1) {
            int dia = 8;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 2) {
            int dia = 15;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 3) {
            int dia = 22;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 4) {
            int dia = 29;
            VentanaCita(dia, mes, anioSeleccionado);
        }
    }//GEN-LAST:event_dia0SemanaActionPerformed

    private void dia2SemanaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia2SemanaActionPerformed
        int indice = cb_semanaSemanal.getSelectedIndex();
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
                mes = 1;
            }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        if (indice <= 0) {
            int dia = 3;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 1) {
            int dia = 10;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 2) {
            int dia = 17;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 3) {
            int dia = 24;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 4) {
            int dia = 31;
            VentanaCita(dia, mes, anioSeleccionado);
        }
    }//GEN-LAST:event_dia2SemanaActionPerformed

    private void dia3SemanaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia3SemanaActionPerformed
        int indice = cb_semanaSemanal.getSelectedIndex();
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
                mes = 1;
            }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        if (indice <= 0) {
            int dia = 4;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 1) {
            int dia = 11;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 2) {
            int dia = 18;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 3) {
            int dia = 25;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 4) {
            int dia = 32;
            VentanaCita(dia, mes, anioSeleccionado);
        }
    }//GEN-LAST:event_dia3SemanaActionPerformed

    private void dia4SemanaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia4SemanaActionPerformed
        int indice = cb_semanaSemanal.getSelectedIndex();
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
                mes = 1;
            }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        if (indice <= 0) {
            int dia = 5;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 1) {
            int dia = 12;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 2) {
            int dia = 19;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 3) {
            int dia = 26;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 4) {
            int dia = 33;
            VentanaCita(dia, mes, anioSeleccionado);
        }
    }//GEN-LAST:event_dia4SemanaActionPerformed

    private void dia5SemanaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia5SemanaActionPerformed
        int indice = cb_semanaSemanal.getSelectedIndex();
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
                mes = 1;
            }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        if (indice <= 0) {
            int dia = 6;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 1) {
            int dia = 13;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 2) {
            int dia = 20;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 3) {
            int dia = 27;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 4) {
            int dia = 34;
            VentanaCita(dia, mes, anioSeleccionado);
        }
    }//GEN-LAST:event_dia5SemanaActionPerformed

    private void dia6SemanaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dia6SemanaActionPerformed
        int indice = cb_semanaSemanal.getSelectedIndex();
        int mes = cb_mesCalendario.getSelectedIndex();
        if (mes <= 0) {
                mes = 1;
            }
        else {
            mes += 1;
        }
        String anio = tf_anioCalendario.getText();
        StringInt nuevoAnio = new StringInt(anio);
        int anioSeleccionado = nuevoAnio.pasar_a_Int();
        if (indice <= 0) {
            int dia = 7;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 1) {
            int dia = 14;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 2) {
            int dia = 21;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 3) {
            int dia = 28;
            VentanaCita(dia, mes, anioSeleccionado);
        }
        if (indice == 4) {
            int dia = 35;
            VentanaCita(dia, mes, anioSeleccionado);
        }
    }//GEN-LAST:event_dia6SemanaActionPerformed

    private void calendarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calendarioActionPerformed
        tf_anioCalendario.setText(Calendar.getInstance().get(Calendar.YEAR) + "");
        cb_mesCalendario.setSelectedIndex(Calendar.getInstance().get(Calendar.MONTH));
        cb_semanaSemanal.setSelectedIndex(0);
        Panel.removeAll();
        Panel.add(panelCalendario);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_calendarioActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void cb_servicioRegistrarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_servicioRegistrarCitaActionPerformed
        int indice = cb_servicioRegistrarCita.getSelectedIndex();
        if (indice < 1) {
            ta_servicioRegistrarCita.setText(sistema.getListaServicios().get(0).getDescripcion());
        }
        else {
            ta_servicioRegistrarCita.setText(sistema.getListaServicios().get(indice).getDescripcion());
        }
    }//GEN-LAST:event_cb_servicioRegistrarCitaActionPerformed

    private void cb_diaRegistrarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_diaRegistrarCitaActionPerformed
        try {
            cb_horasRegistrarCita.removeAllItems();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yyyy");
            String dia = "";
            String mes;
            if (cb_diaRegistrarCita.getSelectedIndex() <= 0) {
                dia = 1 + "";
            }
            else {
                dia = (cb_diaRegistrarCita.getSelectedIndex() + 1) + "";
            }
            if (cb_mesRegistrarCita.getSelectedIndex() <= 0) {
                mes = 1 + "";
            }
            else {
                mes = (cb_mesRegistrarCita.getSelectedIndex() + 1) + "";
            }
            System.out.println(cb_anioRegistrarCita.getSelectedIndex());
            //cb_anioRegistrarCita.setSelectedIndex(0);
            String anio = cb_anioRegistrarCita.getSelectedItem() + "";
            System.out.println(anio);
            LocalDate date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            String diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US); // String = Tue
            System.out.println(diaFinal);
            int inicio = 0;
            int fin = 0;
            if (diaFinal.equals("Mon")) {
                cb_horasRegistrarCita.addItem("Lunes");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(0).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(0).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            else if (diaFinal.equals("Tue")) {
                cb_horasRegistrarCita.addItem("Martes");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(1).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(1).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            else if (diaFinal.equals("Wed")) {
                cb_horasRegistrarCita.addItem("Miercoles");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(2).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(2).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            else if (diaFinal.equals("Thu")) {
                cb_horasRegistrarCita.addItem("Jueves");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(3).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(3).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            else if (diaFinal.equals("Fri")) {
                cb_horasRegistrarCita.addItem("Viernes");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(4).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(4).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            else if (diaFinal.equals("Sat")) {
                cb_horasRegistrarCita.addItem("Sabado");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(5).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(5).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            else if (diaFinal.equals("Sun")) {
                cb_horasRegistrarCita.addItem("Domingo");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(6).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(6).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            if (inicio <= 0) {
                inicio = 0;
            }
            if (fin <= 0) {
                fin = 0;
            }
            while (inicio < fin) {
                cb_horasRegistrarCita.addItem(cb_HorarioInicio.getItemAt(inicio) + "");
                inicio += 1;
            }
        }
        catch (Exception E) {}

    }//GEN-LAST:event_cb_diaRegistrarCitaActionPerformed

    private void cb_mesRegistrarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_mesRegistrarCitaActionPerformed
        cb_diaRegistrarCita.removeAllItems();
        int mes = cb_mesRegistrarCita.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        int anio = Calendar.getInstance().get(Calendar.YEAR) + cb_anioRegistrarCita.getSelectedIndex();
        YearMonth yearMonthObject = YearMonth.of(anio, mes);
        int daysInMonth = yearMonthObject.lengthOfMonth();
        int contador = 1;
        while (contador <= daysInMonth) {
            cb_diaRegistrarCita.addItem(contador + "");
            contador += 1;
        }
    }//GEN-LAST:event_cb_mesRegistrarCitaActionPerformed

    private void cb_anioRegistrarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_anioRegistrarCitaActionPerformed
        cb_diaRegistrarCita.removeAllItems();
        int mes = cb_mesRegistrarCita.getSelectedIndex();
        if (mes <= 0) {
            mes = 1;
        }
        else {
            mes += 1;
        }
        int anio = Calendar.getInstance().get(Calendar.YEAR) + cb_anioRegistrarCita.getSelectedIndex();
        YearMonth yearMonthObject = YearMonth.of(anio, mes);
        int daysInMonth = yearMonthObject.lengthOfMonth();
        int contador = 1;
        while (contador <= daysInMonth) {
            cb_diaRegistrarCita.addItem(contador + "");
            contador += 1;
        }
        //cb_diaRegistrarCita.setSelectedIndex(0);
    }//GEN-LAST:event_cb_anioRegistrarCitaActionPerformed

    private void cb_clienteRegistrarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_clienteRegistrarCitaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb_clienteRegistrarCitaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (cb_clienteRegistrarCita.getSelectedIndex() > 0) {
            if (cb_horasRegistrarCita.getSelectedIndex() > 0) {
                String anioTexto = cb_anioRegistrarCita.getSelectedItem() + "";
                StringInt anioInt = new StringInt(anioTexto);
                int anio = anioInt.pasar_a_Int();
                int mes = cb_mesRegistrarCita.getSelectedIndex();
                if (mes <= 0) {
                    mes = 0;
                }
                int dia = cb_diaRegistrarCita.getSelectedIndex();
                if (dia <= 0) {
                    dia = 1;
                }
                else {
                    dia += 1;
                }
                String hora = cb_horasRegistrarCita.getSelectedItem() + "";
                int indiceCliente = cb_clienteRegistrarCita.getSelectedIndex();
                if (indiceCliente <= 0) {indiceCliente = 0;}
                Cliente cliente = sistema.getListaClientes().get(indiceCliente - 1);
                int indiceServicio = cb_servicioRegistrarCita.getSelectedIndex();
                if (indiceServicio <= 0) {indiceServicio = 0;}
                Servicio servicio = sistema.getListaServicios().get(indiceServicio);
                sistema.crearCita(cliente, anio, mes, dia, hora, servicio);
            }
            else {
                JOptionPane.showMessageDialog(null, "Favor de seleccionar una hora.");
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "Favor seleccione un cliente.");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void consultarServicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consultarServicioActionPerformed
        if (sistema.getListaServicios().size() == 0){
            JOptionPane.showMessageDialog(null, "No hay servicios registrados");
        }else{
          cb_servicioConsulta.removeAllItems();
          cb_servicioConsulta.addItem("");
          for (int i = 0; i < sistema.getListaServicios().size(); i++) {
            cb_servicioConsulta.addItem(sistema.getListaServicios().get(i).getNombre());
          } 
          Panel.removeAll();
          Panel.add(panelConsultarServicio);
          Panel.repaint();
          Panel.revalidate();
        } 
    }//GEN-LAST:event_consultarServicioActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_jButton11ActionPerformed

    private void cb_servicioConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_servicioConsultaActionPerformed
        int indice = cb_servicioConsulta.getSelectedIndex();
        if (indice <= 0) {
            tf_nombreServicioConsulta.setText("");
            ta_descripcionServicioConsulta.setText("");
        }
        else {
            indice -= 1;
            String nombre = sistema.getListaServicios().get(indice).getNombre();
            String descripcion = sistema.getListaServicios().get(indice).getDescripcion();
            tf_nombreServicioConsulta.setText(nombre);
            ta_descripcionServicioConsulta.setText(descripcion);
        }
    }//GEN-LAST:event_cb_servicioConsultaActionPerformed

    private void cb_anioModificarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_anioModificarCitaActionPerformed
        if (cb_anioModificarCita.getSelectedIndex() > 0){
            if (cb_mesModificarCita.getSelectedIndex() > 0){
                int mes = cb_mesModificarCita.getSelectedIndex();
                int anio = Integer.parseInt(cb_anioModificarCita.getSelectedItem().toString());
                YearMonth yearMonthObject = YearMonth.of(anio, mes);
                int daysInMonth = yearMonthObject.lengthOfMonth();
                int dia = 1;
                cb_horaModificarCita.removeAllItems();
                cb_horaModificarCita.addItem("");
                cb_diaModificarCita.removeAllItems();
                cb_diaModificarCita.addItem("");
                while (dia <= daysInMonth) {
                    cb_diaModificarCita.addItem(dia + "");
                    dia += 1;
                }
            }else{
                cb_diaModificarCita.removeAllItems();
                cb_diaModificarCita.addItem("");
                cb_horaModificarCita.removeAllItems();
                cb_horaModificarCita.addItem("");
            }
        }else{
            cb_mesModificarCita.setSelectedItem("");
        }
    }//GEN-LAST:event_cb_anioModificarCitaActionPerformed

    private void cb_mesModificarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_mesModificarCitaActionPerformed
        cb_diaModificarCita.removeAllItems();
        if (cb_anioModificarCita.getSelectedIndex() > 0){
            if (cb_mesModificarCita.getSelectedIndex() > 0){
                int mes = cb_mesModificarCita.getSelectedIndex();
                int anio = Integer.parseInt(cb_anioModificarCita.getSelectedItem().toString());
                YearMonth yearMonthObject = YearMonth.of(anio, mes);
                int daysInMonth = yearMonthObject.lengthOfMonth();
                cb_diaModificarCita.removeAllItems();
                cb_diaModificarCita.addItem("");
                int dia = 1;
                while (dia <= daysInMonth){
                    cb_diaModificarCita.addItem(dia + "");
                    dia += 1;
                }
            }else{
                cb_diaModificarCita.removeAllItems();
                cb_diaModificarCita.addItem("");
            }
                
        }
        
    }//GEN-LAST:event_cb_mesModificarCitaActionPerformed

    private void cb_diaModificarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_diaModificarCitaActionPerformed
        try {
            cb_horaModificarCita.removeAllItems();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/M/yyyy");
            String dia = "";
            String mes;
            if (cb_diaModificarCita.getSelectedIndex() <= 0) {
                dia = 1 + "";
            }
            else {
                dia = (cb_diaModificarCita.getSelectedIndex()) + "";
            }
            if (cb_mesModificarCita.getSelectedIndex() <= 0) {
                mes = 1 + "";
            }
            else {
                mes = (cb_mesModificarCita.getSelectedIndex() + 1) + "";
            }
            System.out.println(cb_anioModificarCita.getSelectedIndex());
            //cb_anioRegistrarCita.setSelectedIndex(0);
            String anio = cb_anioModificarCita.getSelectedItem() + "";
            System.out.println(anio);
            LocalDate date = LocalDate.parse(dia + "/" + mes + "/" + anio, formatter); // LocalDate = 2010-02-23
            String diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US); // String = Tue
            System.out.println(diaFinal);
            int inicio = 0;
            int fin = 0;
            if (diaFinal.equals("Mon")) {
                cb_horaModificarCita.addItem("Lunes");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(0).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(0).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            else if (diaFinal.equals("Tue")) {
                cb_horaModificarCita.addItem("Martes");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(1).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(1).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            else if (diaFinal.equals("Wed")) {
                cb_horaModificarCita.addItem("Miercoles");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(2).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(2).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            else if (diaFinal.equals("Thu")) {
                cb_horaModificarCita.addItem("Jueves");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(3).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(3).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            else if (diaFinal.equals("Fri")) {
                cb_horaModificarCita.addItem("Viernes");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(4).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(4).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            else if (diaFinal.equals("Sat")) {
                cb_horaModificarCita.addItem("Sabado");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(5).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(5).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            else if (diaFinal.equals("Sun")) {
                cb_horaModificarCita.addItem("Domingo");
                cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(6).getInicioHorario());
                inicio = cb_HorarioInicio.getSelectedIndex();
                cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(6).getFinHorario());
                fin = cb_HorarioFin.getSelectedIndex();
            }
            if (inicio <= 0) {
                inicio = 0;
            }
            if (fin <= 0) {
                fin = 0;
            }
            while (inicio < fin) {
                cb_horaModificarCita.addItem(cb_HorarioInicio.getItemAt(inicio) + "");
                inicio += 1;
            }
        }
        catch (Exception E) {}
        /*cb_horaModificarCita.removeAllItems();
        cb_horaModificarCita.addItem("");
        if (cb_diaModificarCita.getSelectedIndex() > 0){
            DateTimeFormatter formato = DateTimeFormatter.ofPattern("d/M/yyyy");
            String dia = cb_diaModificarCita.getSelectedIndex() + "";
            String mes = cb_mesModificarCita.getSelectedIndex() + "";
            String anio = cb_anioModificarCita.getSelectedItem().toString();
            LocalDate date = LocalDate.parse(dia + "/" + mes + "/" + anio, formato);
            String diaFinal = date.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US);
            int diaSemana = 0;
            switch (diaFinal){
                case "Mon":
                    diaSemana = 0;
                    break;
                case "Tue":
                    diaSemana = 1;
                    break;
                case "Wed":
                    diaSemana = 2;
                    break;
                case "Thu":
                    diaSemana = 3;
                    break;
                case "Fri":
                    diaSemana = 4;
                    break;
                case "Sat":
                    diaSemana = 5;
                    break;
                case "Sun":
                    diaSemana = 6;
                    break; 
            }
            cb_HorarioInicio.setSelectedItem(sistema.getListaHorarios().get(diaSemana).getInicioHorario());
            int inicio = cb_HorarioInicio.getSelectedIndex();
            cb_HorarioFin.setSelectedItem(sistema.getListaHorarios().get(diaSemana).getFinHorario());
            int fin = cb_HorarioFin.getSelectedIndex();
            while (inicio < fin){
               cb_horaModificarCita.addItem(cb_HorarioInicio.getItemAt(inicio) + "");
               inicio += 1;
            }
        }else{
            cb_horaModificarCita.removeAllItems();
            cb_horaModificarCita.addItem("");
        }*/
    }//GEN-LAST:event_cb_diaModificarCitaActionPerformed

    private void btn_modificarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_modificarCitaActionPerformed
        // TODO add your handling code here:
        int indice = cb_clienteModificarCita.getSelectedIndex();
        if (indice > 0) {
            int size = sistema.getListaClientes().get(indice - 1).getListadoCitas().size();
            if (size > 0) {
                if (cb_horaModificarCita.getSelectedIndex() > 0) {
                    Cliente cliente = sistema.getListaClientes().get(indice - 1);
                    String anio = cb_anioModificarCita.getSelectedItem() + "";
                    StringInt anioNumero = new StringInt(anio);
                    int anioSeleccionado = anioNumero.pasar_a_Int();
                    int mes = cb_mesModificarCita.getSelectedIndex();
                    if (mes <= 0) {
                        mes = 0;
                    }
                    int dia = cb_diaModificarCita.getSelectedIndex();
                    if (dia <= 0) {
                        dia = 0;
                    }
                    //dia += 1;
                    String hora = cb_horaModificarCita.getSelectedItem() + "";
                    int indiceServicio = cb_serviciosModificarCita.getSelectedIndex();
                    if (indiceServicio <= 0) {indiceServicio = 0;}
                    indiceServicio -= 1;
                    Servicio servicio = sistema.getListaServicios().get(indiceServicio);
                    int indiceCita = cb_citasModificarCita.getSelectedIndex();
                    if (indiceCita <= 0) {indiceCita = 0;}
                    sistema.modificarCita(indiceCita, cliente, anioSeleccionado, mes, dia, hora, indiceServicio);
                    cb_clienteModificarCita.setSelectedIndex(indice);
                    try {
                        cb_citasModificarCita.setSelectedIndex(indiceCita);
                    }
                    catch (Exception E) {}
                    
                }
                else {
                    JOptionPane.showMessageDialog(null, "Favor de seleccionar una hora.");
                }
            }
            else {
                JOptionPane.showMessageDialog(null, "El cliente seleccionado no posee citas para modificar.");
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "Favor de escojer un cliente.");
        }
        /*if (cb_citasModificarCita.getSelectedIndex() > 0){
            if (cb_anioModificarCita.getSelectedIndex() > 0){
                if (cb_mesModificarCita.getSelectedIndex() > 0){
                    if (cb_diaModificarCita.getSelectedIndex() > 0){
                        if (cb_horaModificarCita.getSelectedIndex() > 0){
                            if (cb_serviciosModificarCita.getSelectedIndex() > 0){
                                Panel.removeAll();
                                int anio = Integer.parseInt(cb_anioModificarCita.getSelectedItem().toString());
                                int mes = cb_mesModificarCita.getSelectedIndex();
                                int dia = cb_diaModificarCita.getSelectedIndex();
                                String hora = cb_horaModificarCita.getSelectedItem().toString();
                                int indiceCita = -1;
                                for (int i = 0; i < sistema.getListaClientes().size(); i++){
                                    for (int j = 0; j < sistema.getListaClientes().get(i).getListadoCitas().size(); j++){
                                        if (sistema.getListaClientes().get(i).getListadoCitas().get(j).getAnio() == anio){
                                            if (sistema.getListaClientes().get(i).getListadoCitas().get(j).getMes() == mes){
                                                if (sistema.getListaClientes().get(i).getListadoCitas().get(i).getDia() == dia){
                                                    if (sistema.conseguirHora(hora) == sistema.conseguirHora(sistema.getListaClientes().get(i).getListadoCitas().get(j).getHora())){
                                                        indiceCita = j;
                                                        break;            
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (indiceCita >= 0){
                                    int indiceServicio = cb_serviciosModificarCita.getSelectedIndex() - 1;
                                }
                                cb_citasModificarCita.setSelectedItem("");
                                cb_anioModificarCita.setSelectedItem("");
                                Panel.add(panelModificarCita);
                                Panel.repaint();
                                Panel.revalidate();
                            }
                          
                        }
                        
                        
                    }
                }
            }
        }*/    
    }//GEN-LAST:event_btn_modificarCitaActionPerformed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_jButton13ActionPerformed

    private void modificarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarCitaActionPerformed
        if (sistema.sizeCitas() == 0){
            JOptionPane.showMessageDialog(null, "No hay citas en el sistema");
        }else if (sistema.getListaServicios().size() == 0){
            JOptionPane.showMessageDialog(null, "No hay servicios en el sistema");
        }else{
            Panel.removeAll();
            cb_anioModificarCita.removeAllItems();
            cb_anioModificarCita.addItem("");
            cb_mesModificarCita.removeAllItems();
            cb_mesModificarCita.addItem("");
            cb_mesModificarCita.addItem("Enero");
            cb_mesModificarCita.addItem("Febrero");
            cb_mesModificarCita.addItem("Marzo");
            cb_mesModificarCita.addItem("Abril");
            cb_mesModificarCita.addItem("Mayo");
            cb_mesModificarCita.addItem("Junio");
            cb_mesModificarCita.addItem("Julio");
            cb_mesModificarCita.addItem("Agosto");
            cb_mesModificarCita.addItem("Septiembre");
            cb_mesModificarCita.addItem("Octubre");
            cb_mesModificarCita.addItem("Noviembre");
            cb_mesModificarCita.addItem("Diciembre");
            
            cb_diaModificarCita.removeAllItems();
            cb_diaModificarCita.addItem("");

            cb_citasModificarCita.removeAllItems();
            cb_citasModificarCita.addItem("");
            ta_descripcionModificarCita.setText("");
            
            cb_clienteModificarCita.removeAllItems();
            cb_clienteModificarCita.addItem("");
            for (int i = 0; i < sistema.getListaClientes().size(); i++){
                String entrada = sistema.getListaClientes().get(i).getNombre() + " " +
                        sistema.getListaClientes().get(i).getApellido1() + " " + 
                        sistema.getListaClientes().get(i).getApellido2() + " " + 
                        sistema.getListaClientes().get(i).getTelefono() + "";
                cb_clienteModificarCita.addItem(entrada);
            }
            cb_serviciosModificarCita.removeAllItems();
            cb_serviciosModificarCita.addItem("");
            for (int i = 0; i < sistema.getListaServicios().size(); i++){
              cb_serviciosModificarCita.addItem(sistema.getListaServicios().get(i).getNombre());
            }
            Panel.add(panelModificarCita);
            Panel.repaint();
            Panel.revalidate();
        }
    }//GEN-LAST:event_modificarCitaActionPerformed

    private void cb_serviciosModificarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_serviciosModificarCitaActionPerformed
        int indice = cb_serviciosModificarCita.getSelectedIndex();
        if (indice > 0) {
            ta_descripcionModificarCita.setText(sistema.getListaServicios().get(indice - 1).getDescripcion());
        }
    }//GEN-LAST:event_cb_serviciosModificarCitaActionPerformed

    private void cb_HorarioInicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_HorarioInicioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb_HorarioInicioActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        if (sistema.getListaClientes().size() == 0){
            JOptionPane.showMessageDialog(null, "No hay clientes en el sistema");
        }else{
            Panel.removeAll();
            Panel.add(panelListaEspera);
            Panel.repaint();
            Panel.revalidate();
            cb_listaEspera.removeAllItems();
            cb_listaEspera.addItem("");
            for (int i = 0; i < sistema.getListaClientes().size(); i++){
                String entrada = sistema.getListaClientes().get(i).getNombre() + " " + 
                        sistema.getListaClientes().get(i).getApellido1() + " " + 
                        sistema.getListaClientes().get(i).getApellido2() + " - Tel: " + 
                        sistema.getListaClientes().get(i).getTelefono() + "";
                cb_listaEspera.addItem(entrada);
            }
            listaEspera.removeAllItems();
            for (int i = 0; i < sistema.getListaEspera().size(); i++) {
                listaEspera.addItem(sistema.getListaEspera().get(i).getNombre() + " " +
                        sistema.getListaEspera().get(i).getApellido1() + " - Tel: " +
                        sistema.getListaEspera().get(i).getTelefono());
            }
        }
        
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void tbn_agrgearListaEsperaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tbn_agrgearListaEsperaActionPerformed
        // TODO add your handling code here:
        int indice = cb_listaEspera.getSelectedIndex();
        if (indice > 0){
            sistema.agregarListaEspera(indice);
            listaEspera.removeAllItems();
            for (int i = 0; i < sistema.getListaEspera().size(); i++) {
                listaEspera.addItem(sistema.getListaEspera().get(i).getNombre() + " " +
                        sistema.getListaEspera().get(i).getApellido1() + " - Tel: " +
                        sistema.getListaEspera().get(i).getTelefono());
            }
        }else{
            JOptionPane.showMessageDialog(null, "Debe seleccionar un cliente");
        }
    }//GEN-LAST:event_tbn_agrgearListaEsperaActionPerformed

    private void btn_eliminarListaEsperaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_eliminarListaEsperaActionPerformed
        // TODO add your handling code here:
        if (sistema.getListaEspera().size() == 0){
            JOptionPane.showMessageDialog(null, "No hay clientes en la lista de espera");
        }else{
            int indice = listaEspera.getSelectedIndex();
            if (indice <= 0){indice = 0;}
            sistema.eliminarListaEspera(indice);
            listaEspera.removeAllItems();
            for (int i = 0; i < sistema.getListaEspera().size(); i++) {
                listaEspera.addItem(sistema.getListaEspera().get(i).getNombre() + " " +
                        sistema.getListaEspera().get(i).getApellido1() + " - Tel: " +
                        sistema.getListaEspera().get(i).getTelefono());
            }
        }
    }//GEN-LAST:event_btn_eliminarListaEsperaActionPerformed

    private void btn_cancelarListaEsperaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarListaEsperaActionPerformed
        // TODO add your handling code here:
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_btn_cancelarListaEsperaActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        if (sistema.getListaEspera().size() == 0){
            JOptionPane.showMessageDialog(null, "No hay clientes en lista de espera");
        }else{
            Panel.removeAll();
            Panel.add(panelConsultarListaEspera);
            String texto = "";
            for (int i = 0; i < sistema.getListaEspera().size(); i++){
                texto = texto + sistema.getListaEspera().get(i).getNombre() + " " +
                        sistema.getListaEspera().get(i).getApellido1() + " " +
                        sistema.getListaEspera().get(i).getApellido2() + " " +
                        "\n";
            }
            ta_clienteListaEspera.setText(texto);
            Panel.repaint();
            Panel.revalidate();
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void confirmarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmarCitaActionPerformed
        // TODO add your handling code here:
        if (sistema.sizeCitas() == 0){
            JOptionPane.showMessageDialog(null, "No hay citas en el sistema");
        }else{
            Panel.removeAll();
            Panel.add(panelConfirmarCita);
            cb_confirmarCita.removeAllItems();
            cb_confirmarCita.addItem("");
            for (int i = 0; i < sistema.getListaClientes().size(); i++){
                String entrada = sistema.getListaClientes().get(i).getNombre() + " " + 
                        sistema.getListaClientes().get(i).getApellido1() + " " +
                        sistema.getListaClientes().get(i).getApellido2() + " " +
                        sistema.getListaClientes().get(i).getTelefono() + " ";
                cb_clienteConfirmar.addItem(entrada);
            }
            Panel.repaint();
            Panel.revalidate();  
        }
    }//GEN-LAST:event_confirmarCitaActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
        int indiceCliente = cb_clienteConfirmar.getSelectedIndex();
        int indiceCita = cb_confirmarCita.getSelectedIndex();
        if (indiceCita > 0 && indiceCliente > 0){
            indiceCita -= 1;
            indiceCliente -= 1;
            Cliente cliente = sistema.getListaClientes().get(indiceCliente);
            sistema.confirmarCita(cliente, indiceCita);
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton15ActionPerformed
        // TODO add your handling code here:
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_jButton15ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        if (sistema.getListaClientes().size() == 0){
            JOptionPane.showMessageDialog(null, "No hay clientes en el sistema");
        }else if (sistema.sizeCitas() == 0){
            JOptionPane.showMessageDialog(null, "No hay citas en el sistema");
        }else{
            Panel.removeAll();
            Panel.add(panelEliminarCita);
            cb_eliminarCitaCliente.removeAllItems();
            cb_eliminarCitaCliente.addItem("");
            for (int i = 0; i < sistema.getListaClientes().size(); i++){
                String entrada = sistema.getListaClientes().get(i).getNombre() + " " +
                        sistema.getListaClientes().get(i).getApellido1() + " " + 
                        sistema.getListaClientes().get(i).getApellido2() + " - Tel: " +
                        sistema.getListaClientes().get(i).getTelefono() + "";
                cb_eliminarCitaCliente.addItem(entrada);
            }
            Panel.repaint();
            Panel.revalidate();  
        }
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void btn_eliminarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_eliminarCitaActionPerformed
        // TODO add your handling code here:
        if (cb_eliminarCitaCliente.getSelectedIndex() > 0) {
            int indiceCliente = cb_eliminarCitaCliente.getSelectedIndex();
            int indiceCita = cb_eliminarCitaCliente.getSelectedIndex();
            if (indiceCliente > 0 && indiceCita > 0){
                indiceCliente -= 1;
                Cliente cliente = sistema.getListaClientes().get(indiceCliente);
                indiceCita -=1;
                sistema.eliminarCita(cliente, indiceCita);
                Panel.removeAll();
                Panel.add(panelPrincipal);
                Panel.repaint();
                Panel.revalidate();
            }
            cb_citaEliminarCita.removeAllItems();
            for (int i = 0; i < sistema.getListaClientes().get(indiceCliente).getListadoCitas().size(); i++){
                    String anio = sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(i).getAnio() + "/";
                    String mes = (sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(i).getMes() + 1) + "/";
                    String dia = sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(i).getDia() + " | ";
                    String hora = sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(i).getHora();
                    String entrada = anio + mes + dia + hora;
                    cb_citaEliminarCita.addItem(entrada);
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar un cliente primero.");
        }
        
    }//GEN-LAST:event_btn_eliminarCitaActionPerformed

    private void cb_citasModificarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_citasModificarCitaActionPerformed
        
        int indiceCliente = cb_clienteModificarCita.getSelectedIndex();
        int indiceCita = cb_citasModificarCita.getSelectedIndex();
        if (indiceCita > 0 && indiceCliente > 0){
            indiceCita -= 1;
            indiceCliente -= 1;
            System.out.println(sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(indiceCita).getTipoServicio().getNombre());
            cb_serviciosModificarCita.setSelectedItem(sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(indiceCita).getTipoServicio().getNombre());
            ta_descripcionModificarCita.setText(sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(indiceCita).getTipoServicio().getDescripcion());
            int anio = Calendar.getInstance().get(Calendar.YEAR);
            int limite = anio + 10;
            while (anio <= limite) {
                cb_anioModificarCita.addItem(anio + "");
                anio += 1;
            }
            
            cb_anioModificarCita.setSelectedItem(sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(indiceCita).getAnio() + "");
            cb_mesModificarCita.setSelectedIndex(sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(indiceCita).getMes());
            cb_diaModificarCita.setSelectedIndex(sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(indiceCita).getDia());
            cb_horaModificarCita.setSelectedItem(sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(indiceCita).getHora().toString());
        }else{
            cb_anioModificarCita.removeAllItems();
            cb_anioModificarCita.addItem("");
            cb_mesModificarCita.setSelectedItem("Enero");
            cb_diaModificarCita.removeAllItems();
            cb_diaModificarCita.addItem("");
            cb_horaModificarCita.removeAllItems();
            cb_horaModificarCita.addItem("");
            cb_serviciosModificarCita.setSelectedItem("");
            ta_descripcionModificarCita.setText("");
        }
        
    }//GEN-LAST:event_cb_citasModificarCitaActionPerformed

    private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton19ActionPerformed
        // TODO add your handling code here:
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_jButton19ActionPerformed

    private void cb_eliminarCitaClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_eliminarCitaClienteActionPerformed
        // TODO add your handling code here:
        if (cb_eliminarCitaCliente.getSelectedIndex() > 0){
            int indiceCliente = cb_eliminarCitaCliente.getSelectedIndex()-1;
            if (sistema.getListaClientes().get(indiceCliente).getListadoCitas().size() == 0){
                JOptionPane.showMessageDialog(null, "Ese cliente no tiene citas");
            }else{
                cb_citaEliminarCita.removeAllItems();
                for (int i = 0; i < sistema.getListaClientes().get(indiceCliente).getListadoCitas().size(); i++){
                    String anio = sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(i).getAnio() + "/";
                    String mes = (sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(i).getMes() + 1) + "/";
                    String dia = sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(i).getDia() + " | ";
                    String hora = sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(i).getHora();
                    String entrada = anio + mes + dia + hora;
                    cb_citaEliminarCita.addItem(entrada);
                }
            }
        }else{
            cb_citaEliminarCita.removeAllItems();
            cb_citaEliminarCita.addItem("");
        }
    }//GEN-LAST:event_cb_eliminarCitaClienteActionPerformed

    private void cb_clienteModificarCitaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_clienteModificarCitaActionPerformed
        // TODO add your handling code here:
        int indiceCliente = cb_clienteModificarCita.getSelectedIndex();
        if (indiceCliente > 0){
            indiceCliente -= 1;
            cb_citasModificarCita.removeAllItems();
            cb_citasModificarCita.addItem("");
            for (int i = 0; i < sistema.getListaClientes().get(indiceCliente).getListadoCitas().size(); i++){
                    String anio = sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(i).getAnio() + "/";
                    String mes = (sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(i).getMes() + 1) + "/";
                    String dia = sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(i).getDia() + " | ";
                    String hora = sistema.getListaClientes().get(indiceCliente).getListadoCitas().get(i).getHora();
                    String entrada = anio + mes + dia + hora;
                    cb_citasModificarCita.addItem(entrada);
                }
        }
        else {
            cb_anioModificarCita.removeAllItems();
            cb_mesModificarCita.removeAllItems();
            cb_mesModificarCita.addItem("Enero");
            cb_mesModificarCita.addItem("Febrero");
            cb_mesModificarCita.addItem("Marzo");
            cb_mesModificarCita.addItem("Abril");
            cb_mesModificarCita.addItem("Mayo");
            cb_mesModificarCita.addItem("Junio");
            cb_mesModificarCita.addItem("Julio");
            cb_mesModificarCita.addItem("Agosto");
            cb_mesModificarCita.addItem("Septiembre");
            cb_mesModificarCita.addItem("Octubre");
            cb_mesModificarCita.addItem("Noviembre");
            cb_mesModificarCita.addItem("Diciembre");
            cb_diaModificarCita.removeAllItems();
            cb_citasModificarCita.removeAllItems();
            ta_descripcionModificarCita.setText("");
        }
    }//GEN-LAST:event_cb_clienteModificarCitaActionPerformed

    private void cb_clienteConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_clienteConfirmarActionPerformed
        // TODO add your handling code here:
        if (cb_clienteConfirmar.getSelectedIndex() > 0){
            int indiceCliente = cb_clienteConfirmar.getSelectedIndex();
            indiceCliente -= 1;
            if (sistema.getListaClientes().get(indiceCliente).getListadoCitas().size() == 0){
                JOptionPane.showMessageDialog(null, "Este cliente no tiene citas");
            }else{
                cb_confirmarCita.removeAllItems();
                cb_confirmarCita.addItem("");
                for (int i = 0; i < sistema.getListaClientes().size(); i++){
                    for (int j = 0; j < sistema.getListaClientes().get(i).getListadoCitas().size(); j++){
                        String entrada = sistema.getListaClientes().get(i).getListadoCitas().get(j).getAnio() + "-" +
                            sistema.getListaClientes().get(i).getListadoCitas().get(j).getMes() + "-" +
                            sistema.getListaClientes().get(i).getListadoCitas().get(j).getHora() + " | ";
                        entrada = entrada + sistema.getListaClientes().get(i).getListadoCitas().get(j).getTipoServicio().getNombre() + " | ";
                        cb_confirmarCita.addItem(entrada);
                    } 
                }  
            }
            
        }
    }//GEN-LAST:event_cb_clienteConfirmarActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // TODO add your handling code here:
        if (sistema.getListaClientes().size() == 0){
            JOptionPane.showMessageDialog(null, "No hay clientes registrados");
        }else if (sistema.sizeCitas() == 0){
            JOptionPane.showMessageDialog(null, "No hay citas en el sistema");
        }else{
            Panel.removeAll();
            cb_correo.removeAllItems();
            cb_correo.addItem("");
            for (int i = 0; i < sistema.getListaClientes().size(); i++){
                String entrada = sistema.getListaClientes().get(i).getNombre() + " " +
                        sistema.getListaClientes().get(i).getApellido1() + " " + 
                        sistema.getListaClientes().get(i).getApellido2();
                cb_correo.addItem(entrada);
            }
            tf_CorreoAsunto.setText("");
            tf_Correocorreo.setText("");
            ta_CorreoDescripcion.setText("");
            Panel.add(panelCorreo);
            Panel.repaint();
            Panel.revalidate();
            
        }
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void cb_correoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_correoActionPerformed
        // TODO add your handling code here:
        int indice = cb_correo.getSelectedIndex();
        if (indice > 0){
            indice -= 1;
            tf_Correocorreo.setText(sistema.getListaClientes().get(indice).getCorreo());
        }else{
            tf_Correocorreo.setText("");
        }
        tf_CorreoAsunto.setText("");
        ta_CorreoDescripcion.setText("");
    }//GEN-LAST:event_cb_correoActionPerformed

    private void btn_mandarCorreoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_mandarCorreoActionPerformed
        // TODO add your handling code here:
        int indice = cb_correo.getSelectedIndex();
        if (indice == 0){
            JOptionPane.showMessageDialog(null, "Debe escoger un cliente primero");
        }else if (tf_Correocorreo.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "No hay un correo destino");
        }else{
            c.setContrasenia("wggzuukopktvpgar");
            c.setUsuarioCorreo("prograbarberia1@gmail.com");
            c.setAsunto(tf_CorreoAsunto.getText());
            c.setMensaje(ta_CorreoDescripcion.getText());
            c.setDestino(tf_Correocorreo.getText());
            c.setNombreArchivo("fondoPrincipal.png");
            c.setRutaArchivo("fondoPrincipal.png");
            if(sistema.enviarCorreo(c)){
                JOptionPane.showMessageDialog(null, "Se logro enviar el correo");
            
            }else{
                JOptionPane.showMessageDialog(null, "No se pudo enviar el correo");        
            }
        }
    }//GEN-LAST:event_btn_mandarCorreoActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        Panel.removeAll();
        Panel.add(panelPrincipal);
        Panel.repaint();
        Panel.revalidate();
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel Panel;
    private javax.swing.JMenuBar barraOperaciones;
    private javax.swing.JButton btn_agregarServicio;
    private javax.swing.JButton btn_cancelaModificarCliente;
    private javax.swing.JButton btn_cancelarConsultarCliente;
    private javax.swing.JButton btn_cancelarEliminarCliente;
    private javax.swing.JButton btn_cancelarListaEspera;
    private javax.swing.JButton btn_cancelarRegistrarCliente;
    private javax.swing.JButton btn_eliminarCita;
    private javax.swing.JButton btn_eliminarCliente;
    private javax.swing.JButton btn_eliminarListaEspera;
    private javax.swing.JButton btn_eliminarServicio;
    private javax.swing.JButton btn_guardarHorario;
    private javax.swing.JButton btn_mandarCorreo;
    private javax.swing.JButton btn_modificarCita;
    private javax.swing.JButton btn_modificarCliente;
    private javax.swing.JButton btn_modificarServicio;
    private javax.swing.JButton btn_registrarCliente;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JMenuItem calendario;
    private javax.swing.JComboBox cb_HorarioFin;
    private javax.swing.JComboBox cb_HorarioInicio;
    private javax.swing.JComboBox<String> cb_anioModificarCita;
    private javax.swing.JComboBox<String> cb_anioRegistrarCita;
    private javax.swing.JComboBox cb_citaEliminarCita;
    private javax.swing.JComboBox<String> cb_citasModificarCita;
    private javax.swing.JComboBox cb_clienteConfirmar;
    private javax.swing.JComboBox cb_clienteModificarCita;
    private javax.swing.JComboBox<String> cb_clienteRegistrarCita;
    private javax.swing.JComboBox cb_confirmarCita;
    private javax.swing.JComboBox<String> cb_consultarCliente;
    private javax.swing.JComboBox cb_correo;
    private javax.swing.JComboBox<String> cb_diaModificarCita;
    private javax.swing.JComboBox<String> cb_diaRegistrarCita;
    private javax.swing.JComboBox cb_eliminarCitaCliente;
    private javax.swing.JComboBox<String> cb_eliminarCliente;
    private javax.swing.JComboBox<String> cb_horaModificarCita;
    private javax.swing.JComboBox cb_horarioDia;
    private javax.swing.JComboBox<String> cb_horasRegistrarCita;
    private javax.swing.JComboBox cb_listaEspera;
    private javax.swing.JComboBox<String> cb_manejarServicios;
    private javax.swing.JComboBox<String> cb_mesCalendario;
    private javax.swing.JComboBox<String> cb_mesModificarCita;
    private javax.swing.JComboBox<String> cb_mesRegistrarCita;
    private javax.swing.JComboBox<String> cb_mesSemanal;
    private javax.swing.JComboBox<String> cb_modificarCliente;
    private javax.swing.JComboBox<String> cb_semanaSemanal;
    private javax.swing.JComboBox<String> cb_servicioConsulta;
    private javax.swing.JComboBox<String> cb_servicioRegistrarCita;
    private javax.swing.JComboBox<String> cb_serviciosModificarCita;
    private javax.swing.JMenuItem confirmarCita;
    private javax.swing.JMenuItem consultarCliente;
    private javax.swing.JMenuItem consultarServicio;
    private javax.swing.JMenuItem crearCita;
    private javax.swing.JButton dia0;
    private javax.swing.JButton dia0Semana;
    private javax.swing.JButton dia1;
    private javax.swing.JButton dia10;
    private javax.swing.JButton dia11;
    private javax.swing.JButton dia12;
    private javax.swing.JButton dia13;
    private javax.swing.JButton dia14;
    private javax.swing.JButton dia15;
    private javax.swing.JButton dia16;
    private javax.swing.JButton dia17;
    private javax.swing.JButton dia18;
    private javax.swing.JButton dia19;
    private javax.swing.JButton dia1Semana;
    private javax.swing.JButton dia2;
    private javax.swing.JButton dia20;
    private javax.swing.JButton dia21;
    private javax.swing.JButton dia22;
    private javax.swing.JButton dia23;
    private javax.swing.JButton dia24;
    private javax.swing.JButton dia25;
    private javax.swing.JButton dia26;
    private javax.swing.JButton dia27;
    private javax.swing.JButton dia28;
    private javax.swing.JButton dia29;
    private javax.swing.JButton dia2Semana;
    private javax.swing.JButton dia3;
    private javax.swing.JButton dia30;
    private javax.swing.JButton dia3Semana;
    private javax.swing.JButton dia4;
    private javax.swing.JButton dia4Semana;
    private javax.swing.JButton dia5;
    private javax.swing.JButton dia5Semana;
    private javax.swing.JButton dia6;
    private javax.swing.JButton dia6Semana;
    private javax.swing.JButton dia7;
    private javax.swing.JButton dia8;
    private javax.swing.JButton dia9;
    private javax.swing.JMenuItem eliminarCliente;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JMenuItem horarioSistema;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton17;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton40;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JComboBox<String> listaEspera;
    private javax.swing.JMenuItem manejarServicios;
    private javax.swing.JMenu menuAdministracion;
    private javax.swing.JMenu menuCitas;
    private javax.swing.JMenu menuClientes;
    private javax.swing.JTabbedPane menuPanelCambio;
    private javax.swing.JMenu menuServicios;
    private javax.swing.JMenuItem modificarCita;
    private javax.swing.JMenuItem modificarCliente;
    private javax.swing.JPanel panelCalendario;
    private javax.swing.JPanel panelConfirmarCita;
    private javax.swing.JPanel panelConsultarCliente;
    private javax.swing.JPanel panelConsultarListaEspera;
    private javax.swing.JPanel panelConsultarServicio;
    private javax.swing.JPanel panelCorreo;
    private javax.swing.JPanel panelCrearCita;
    private javax.swing.JPanel panelEliminarCita;
    private javax.swing.JPanel panelEliminarCliente;
    private javax.swing.JPanel panelHorario;
    private javax.swing.JPanel panelListaEspera;
    private javax.swing.JPanel panelManejoServicios;
    private javax.swing.JPanel panelModificarCita;
    private javax.swing.JPanel panelModificarCliente;
    private javax.swing.JPanel panelPrincipal;
    private javax.swing.JPanel panelRegistrarCliente;
    private javax.swing.JMenuItem registrarCliente;
    private javax.swing.JPopupMenu.Separator separadorCliente;
    private javax.swing.JTextArea ta_CorreoDescripcion;
    private javax.swing.JTextArea ta_clienteListaEspera;
    private javax.swing.JTextArea ta_descripcionModificarCita;
    private javax.swing.JTextArea ta_descripcionServicio;
    private javax.swing.JTextArea ta_descripcionServicioConsulta;
    private javax.swing.JTextArea ta_servicioRegistrarCita;
    private javax.swing.JButton tbn_agrgearListaEspera;
    private javax.swing.JTextField tf_CorreoAsunto;
    private javax.swing.JTextField tf_Correocorreo;
    private javax.swing.JTextField tf_anioCalendario;
    private javax.swing.JTextField tf_anioCalendario1;
    private javax.swing.JTextField tf_consultarClienteApellido1;
    private javax.swing.JTextField tf_consultarClienteApellido2;
    private javax.swing.JTextField tf_consultarClienteCorreo;
    private javax.swing.JTextField tf_consultarClienteNombre;
    private javax.swing.JTextField tf_consultarClienteTelefono;
    private javax.swing.JTextField tf_eliminarApellido1Cliente;
    private javax.swing.JTextField tf_eliminarApellido2Cliente;
    private javax.swing.JTextField tf_eliminarCorreoCliente;
    private javax.swing.JTextField tf_eliminarNombreCliente;
    private javax.swing.JTextField tf_eliminarTelefonoCliente;
    private javax.swing.JTextField tf_modificarApellido1Cliente;
    private javax.swing.JTextField tf_modificarApellido2Cliente;
    private javax.swing.JTextField tf_modificarCorreoCliente;
    private javax.swing.JTextField tf_modificarNombreCliente;
    private javax.swing.JTextField tf_modificarTelefonoCliente;
    private javax.swing.JTextField tf_nombreServicio;
    private javax.swing.JTextField tf_nombreServicioConsulta;
    private javax.swing.JTextField tf_registrarApellido1Cliente;
    private javax.swing.JTextField tf_registrarApellido2Cliente;
    private javax.swing.JTextField tf_registrarCorreoCliente;
    private javax.swing.JTextField tf_registrarNombreCliente;
    private javax.swing.JTextField tf_registrarTelefonoCliente;
    private javax.swing.JPanel vistaMensual;
    private javax.swing.JPanel vistaSemanal;
    // End of variables declaration//GEN-END:variables
}
