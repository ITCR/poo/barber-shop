
package Objetos;

import java.io.Serializable;
import java.util.ArrayList;


public class Cliente implements Serializable{
    
    private String nombre;
    private String apellido1;
    private String apellido2;
    private String correo;
    private int telefono;
    private ArrayList <Cita> listadoCitas;
    
    public Cliente(String pNombre, String pApellido1, String pApellido2, String pCorreo, int pTelefono) {
        nombre = pNombre;
        apellido1 = pApellido1;
        apellido2 = pApellido2;
        correo = pCorreo;
        telefono = pTelefono;
        listadoCitas = new ArrayList();
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public String getCorreo() {
        return correo;
    }

    public int getTelefono() {
        return telefono;
    }
    
    public ArrayList<Cita> getListadoCitas() {
      return listadoCitas;
    }
    
    public void addCita(Cita pCita) {
        listadoCitas.add(pCita);
    }
    
    public void removeCita(Cita pCita) {
      listadoCitas.remove(pCita);
    }
    
    public boolean verificarCampo(Cita pCita){
        boolean noTieneCita = true;
        for (int i = 0; i < listadoCitas.size(); i++){
            if (listadoCitas.get(i).getAnio() == pCita.getAnio()){    
                if (listadoCitas.get(i).getMes() == pCita.getMes()){
                    if (listadoCitas.get(i).getDia() == pCita.getDia()){
                        if (listadoCitas.get(i).getHora().equalsIgnoreCase(pCita.getHora())){
                            noTieneCita = false;
                            break;
                        }
                    }
                }
            }
        }
        return noTieneCita;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
 
}
