
package Objetos;

import java.io.Serializable;


public class Cita implements Serializable{
   
    private int anio;
    private int mes;
    private int dia;
    private String hora;
    private Servicio tipoServicio;
    private Cliente cliente;
    private boolean confirmado;
    
    public Cita(Cliente pCliente, int pAnio, int pMes, int pDia, String pHora, Servicio pServicio) {
        cliente = pCliente;
        anio = pAnio;
        mes = pMes;
        dia = pDia;
        hora = pHora;
        tipoServicio = pServicio;
        confirmado = false; 
    }
    
    public Cliente getCliente() {
        return cliente;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public Servicio getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(Servicio tipoServicio) {
        this.tipoServicio = tipoServicio;
    }
    
    public void setMes(int pMes){
        mes = pMes;
    }
    public int getMes() {
        return mes;
    }

    public void setDia(int pDia){
        dia = pDia;
    }
    
    public int getDia(){
        return dia;
    }
    
    public void confirmarCita(){
        confirmado = true;
    }
    
    public boolean isConfirmado() {
        return confirmado;
    }
    
    public void setHora(String pHora){
        hora = pHora;
    }
    
    public String getHora(){
        return hora;
    }
    
}
