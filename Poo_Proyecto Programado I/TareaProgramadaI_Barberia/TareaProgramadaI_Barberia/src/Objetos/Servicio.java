
package Objetos;

import java.io.Serializable;


public class Servicio implements Serializable{
    
    private String nombre;
    private String descripcion;
    
    public Servicio(String pNombre, String pDescripcion) {
        nombre = pNombre;
        descripcion = pDescripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
