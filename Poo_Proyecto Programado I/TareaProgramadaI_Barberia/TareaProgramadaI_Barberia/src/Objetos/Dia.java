
package Objetos;

import java.io.Serializable;


public class Dia implements Serializable{
    
    private String inicioHorario;
    private String finHorario;
    
    public Dia(String pInicioHorario, String pFinHorario) {
        inicioHorario = pInicioHorario;
        finHorario = pFinHorario;
    }

    public String getInicioHorario() {
        return inicioHorario;
    }

    public String getFinHorario() {
        return finHorario;
    }

    public void setInicioHorario(String inicioHorario) {
        this.inicioHorario = inicioHorario;
    }

    public void setFinHorario(String finHorario) {
        this.finHorario = finHorario;
    }

}
